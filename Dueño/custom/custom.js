//All ready!. Page &  Cordova loaded.
//Todo listo!. Página & Cordova cargados.
//var server = 'http://192.168.1.191:3000/'
//var server = 'http://192.168.43.79:3000/'
var server = 'http://190.64.48.66:3000/'
//var server = 'https://nocheros-api.mybluemix.net/'
//var server = 'http://localhost:3000/'
let message_moved = false
var promos = []
var clubName = "Rancho"
var currentBar ={}
function deviceReady() {
	try {
		//Example when Internet connection is needed but not mandatory
		//Ejemplo de cuando se necesita conexióna a Internet pero no es obligatoria.
		if (!mui.connectionAvailable()) {
			if ('plugins' in window && 'toast' in window.plugins)
				mui.toast('We recommend you connect your device to the Internet');
			else
				mui.alert('We recommend you connect your device to the Internet');
		}

		//Install events, clicks, resize, online/offline, etc. 
		installEvents();
		installEvents2();
		
		//Hide splash.
		//Ocultar el splash.
		if (navigator.splashscreen) {
			navigator.splashscreen.hide();
		}

	} catch (e) {
		//your decision
		//tu decisión
	}
}

/**
 * Install events, clicks, resize, online/offline, etc., on differents HTML elements.
 * Instala eventos, clicks, resize, online/offline, etc., sobre diferentes elementos HTML.
 */
function installEvents() {

	mui.util.installEvents([
		//Mail list click/touch events. See that if the event is not specified, click is assumed.
		{
			id: '.mui-backarrow',	//Important!
			fn: () => {
				mui.history.back();
				return false;
			}
		},
		{
			id: '#delete-me',
			ev: 'click',	//If not, it assumes click
			fn: () => {
				mui.viewport.showPage("template-page", "DEF");
				return false;
			}
		},
		//MobileUI viewport specific event.
		{
			vp: mui.viewport,
			ev: 'swiperight',
			fn: () => {
				if (!mui.viewport.panelIsOpen()) {
					mui.history.back();
				}
			}
		},
		//It's a good idea to consider what happens when the device is switched on and off the internet.
		//Es buena idea considerar que pasa cuando el dispositivo se conecta y desconecta a Internet.
		{
			id: document,
			ev: 'online',
			fn: () => {
				//Do something
			}
		},
		{
			id: document,
			ev: 'offline',
			fn: () => {
				//Do something
			}
		},
		//Typically fired when the device changes orientation.
		//Típicamente disparado cuando el dispositivo cambia de orientación.
		{
			id: window,
			ev: 'resize',
			fn: () => {
				//Do something if you need
			}
		},
		{
			id: '#login_btn',
			ev: 'click',
			fn: function (e) {
				var email = $("#email_input").val();
				var password = $("#password_input").val();
				authenticate(email, password);
			}
		},
		{
			id: '#account_btn',
			ev: 'click',
			fn: function (response) {
				mui.viewport.showPage('registry-page', "DEF")
			}
		},
		{
			id: '#registry_btn',
			ev: 'click',
			fn: function () {
				var barmanName = $("#name_input_registry").val();
				var password = $("#password_input_registry").val();
				var re_password = $("#re_password_input_registry").val();
				var usertype = $("#usertype_input_registry").val();
				var email = $("#email_input_registry").val();
				registry(email, barmanName, password, re_password, usertype);
			}
		},
		{
			id: '#add_promo',
			ev: 'click',
			fn: function () {
				mui.viewport.showPage('promo-page', "SLIDE_UP")
			}
		},
		{
			id: '#newPromo',
			ev: 'click',
			fn: function () {
				var final_hour = $("#combo_final_hour").val();
				var final_minutes = $("#combo_final_minute").val();
				var init_hour = $("#combo_init_hour").val();
				var init_minutes = $("#combo_init_minute").val();
				var description = $("#description_input").val();
				var promoName = $("#title_input").val();
				var barmanId = currentBar.email//sacar de la sesion
				var clubId = currentBar.clubId//sacar de la sesion
				var picture = $("#title_input").val()
				console.log(barmanId)
				addPromo(final_hour, final_minutes, init_hour, init_minutes, description, barmanId, clubId, promoName, picture)

			}
		},
		{
			id: message,
			ev: 'click',
			fn: function () {

				if(!message_moved){
					$('#footer').css('transition','all 1s')
					$('#footer').css('transform','translate(83%,118%)')
					$('#back-message-arrow').css('transition','all 1s')
					$('#back-message-arrow').css('transform','translateX(-100%)')
					$('#send-msg-div').css('transition','all 1s')
					$('#send-msg-div').css('transform','translateX(100%)')
					message_moved = true
				}else{
					$('#footer').css('transition','all 1s')
					$('#footer').css('transform','translate(0%,0%)')
					$('#back-message-arrow').css('transition','all 1s')
					$('#back-message-arrow').css('transform','translateX(100%)')
					$('#send-msg-div').css('transition','all 1s')
					$('#send-msg-div').css('transform','translateX(-100%)')
					var msg = document.getElementById("send-msg-input").value;
					document.getElementById("send-msg-input").value = "";
					if (!(msg === "")) {
						sendPushMessage(msg);
					}
					message_moved = false
				}
			}
		},
		{
			id: '#back-message-arrow',
			ev: 'click',
			fn: function () {
				$('#footer').css('transition', 'all 1.7s')
				$('#footer').css('transform', 'translate(0%,0%)')
				$('#back-message-arrow').css('transition', 'all 1.7s')
				$('#back-message-arrow').css('transform', 'translateX(100%)')
				$('#send-msg-div').css('transition', 'all 1.7s')
				$('#send-msg-div').css('transform', 'translateX(-100%)')
				message_moved = false
			}
		},
		{
			id: scan_qr,
			ev: 'click',
			fn: function () {
				readQR();
			}
		}, {
			id: '.options',
			ev: 'click',
			fn: function () {
				mui.screen.showPanel('option-panel', 'SLIDE_RIGHT');
			}
		}, {
			id: '#userProfile',
			ev: 'click',
			fn: function () {
				document.getElementById('option-panel').style.visibility = 'hidden'
				mui.viewport.showPage("user-profile-page", 'SLIDE_LEFT')
			}
		}, {
			id: '#mainPage',
			ev: 'click',
			fn: function () {
				document.getElementById('option-panel').style.visibility = 'hidden'
				mui.viewport.showPage("main-page", 'SLIDE_LEFT')
			}
		}
	]);
}
function installEvents2() {
	document.getElementById('upload-image-camera').addEventListener('click', function () {
		var cameraOptions = {
			destinationType: Camera.DestinationType.DATA_URL,
		}
		navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
	})
	document.getElementById('upload-image-gallery').addEventListener('click', function () {
		var cameraOptions = {
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
		}
		navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
	})
}

function compleateHeader(barName, barLogo) {
	for (i in document.getElementsByClassName('bar_name'))
		document.getElementsByClassName('bar_name')[i].innerHTML = "" + barName
	for (j in document.getElementsByClassName('bar_photo'))
		document.getElementsByClassName('bar_photo')[j].innerHTML = `
		<img src ="`+ barLogo + `" class=photo_img>`
	//document.getElementById('bar_photo').style.backgroundImage = 'url(./custom/images/'+barLogo+')'
}
function completeUserProfile(barmanName, barLogo) {
	document.getElementById('main_profile_image').innerHTML = `<img src ="` + barLogo + `" class=photo_img>`
	document.getElementById('name_description').innerHTML = "" + barmanName
}
function authenticate(email, password) {

	$.ajax({
		type: 'POST',
		url: '' + server + 'api/barman/signin',
		data: {
			'email': email,
			'password': password
		},
		success: function (res) {
			document.getElementById('userSkills').innerHTML = `
					<div id=name_panel>`+ res.barmanName + `</div>
					<img src=`+ res.picture + ` id=user_photo_panel>`
			acquirePromos(email, res.clubId)
			console.log(res.picture)
			compleateHeader(res.barmanName, res.picture)
			completeUserProfile(res.barmanName, res.picture)
			loadImages()
			currentBar = res
			console.log("HOLA")
			console.log(currentBar)
			mui.viewport.showPage("main-page", "SLIDE_LEFT");

		},
		error: function (e) {
			window.plugins.toast.showWithOptions({
				message: "El usuario o la contraseña ingresados son incorrectos.",
				duration: "short", // 2000 ms
				position: "bottom",
				styling: {
					opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
					backgroundColor: '#F78181', // make sure you use #RRGGBB. Default #333333
					textColor: '#8A0808', // Ditto. Default #FFFFFF
					textSize: 18, // Default is approx. 13.
					cornerRadius: 3, // minimum is 0 (square). iOS default 20, Android default 100
					horizontalPadding: 20, // iOS default 16, Android default 50
					verticalPadding: 16 // iOS default 12, Android default 30
				}
			});
		}

	})
}
function registry(email, barmanName, password, re_password, usertype) {
	$.ajax({
		type: 'POST',
		url: '' + server + 'api/barman/signup',
		data: {
			'email': email,
			'barmanName': barmanName,
			'password': password,
			're_password': re_password, //confirmacion password
			'usertype': usertype
		},
		success: function () {
			mui.viewport.showPage("main-page", "SLIDE_LEFT");
		},
		error: function (e) {
			alert("error");

			console.log(e);
		}

	})
}
/*function adquirePromosForClub(email){
	$.ajax({
		url: ''+server + 'api/'+email+'/promos',
		data: {
		},
		success: function(data){
			console.log(data)
		},
		error: function(e){
			alert("error");

			console.log(e);
		}
		
	})
}*/
function acquirePromos(barmanId, clubId) {
	//prueba();
	console.log(clubId)
	var httpAcquirePromos = new XMLHttpRequest();
	var respObj;
	httpAcquirePromos.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			respObj = JSON.parse(this.responseText);
			var promosHTML = ''
			for (index in respObj) {
				var promo = respObj[index];
				promos.push(promo);
				var start_time_hours = new Date(promo.start_time).getHours()
				var start_time_minutes = new Date(promo.start_time).getMinutes()
				var finish_time_hours = new Date(promo.finish_time).getHours()
				var finish_time_minutes = new Date(promo.finish_time).getMinutes()
				if (start_time_hours < 10) {
					start_time_hours = '0' + start_time_hours
				}
				if (start_time_minutes < 10) {
					start_time_minutes = '0' + start_time_minutes
				}
				if (finish_time_hours < 10) {
					finish_time_hours = '0' + finish_time_hours
				}
				if (finish_time_hours < 10) {
					finish_time_hours = '0' + finish_time_hours
				}
				promosHTML +=
					`<div class="promo">
<<<<<<< HEAD
						<div id="icon"><img id="icon_img" src="./custom/images/drink_type/`+promo.picture+`" alt=""></div>
=======
						<div id="icon"><img id="icon_img" src="`+ promo.picture + `" alt=""></div>
>>>>>>> a42c57164ac741c1901f55081fa9661240dba881
						<div id="name_promo">`+ promo.name + `</div>
						<div id="drag_place"></div>
						<div id="description_time">`+ start_time_hours + ':' + start_time_minutes + ` a ` + finish_time_hours + ':' + finish_time_minutes + `</div>		
			</div>`
			}
			promosHTML += `<button id="add_promo"></button>`
			console.log(promosHTML)
			document.getElementById('promo_container').innerHTML = promosHTML;
			mui.util.installEvents([{
				id: '#add_promo',
				ev: 'click',
				fn: function () {
					mui.viewport.showPage('promo-page', "SLIDE_UP")
				}
			}])
		}
	}
	httpAcquirePromos.open('GET', server + 'api/clubs/' + barmanId + '/' + clubId + '/promos', true);
	httpAcquirePromos.send();
	return false;
}
function addPromo(final_hour, final_minutes, init_hour, init_minutes, description, barmanId, clubId, promoName, picture) {
	$.ajax({
		type: 'POST',
		url: '' + server + 'api/addPromo',
		data: {
			'final_hour': final_hour,
			'final_minutes': final_minutes,
			'init_hour': init_hour,
			'init_minutes': init_minutes, //confirmacion password
			'description': description,
			'promoName': promoName,
			'clubId': clubId,
			'barmanId': barmanId,
			'picture': picture
		},
		success: function () {
			acquirePromos(barmanId, clubId)
			mui.viewport.showPage("main-page", "SLIDE_LEFT");
		},
		error: function (e) {
			alert("error");

			console.log(e);
		}

	})
}
function sendPushMessage(textMsg) {
	console.log("enviando")
	$.ajax({
		type: 'POST',
		url: '' + server + 'api/sendPush',
		data: {
			msg: textMsg
		},
		success: function () {
			console.log("msg enviado" + textMmsg)
		},
		error: function (e) {
			console.log(e)
		}
	})
}
function readQR() {
	scanQR(function (decodedQR) {
		alert(decodedQR);
		//Acá iría la lógica
	})
}
function loadImages() {
	$.ajax({
		type: 'POST',
		url: server + 'api/clubs/getImages',
		data: {
			name: clubName
		},
		success: function (msg, status, jqXHR) {
			var images = msg.split(',')
			var html = '';
			for (index in images)
				html += '<div><img src="' + images[index] + '" class="photo_item_img" ></div>'
			document.getElementById('photo_container').innerHTML = html;
		},
		error: function (e) {
			console.log(e);
		}

	})
}
function cameraSuccess(data) {
	$.ajax({
		url: server + 'api/uploadImage',
		type: 'POST',
		crossDomain: true,
		data: {
			photo: data,
			clubName: "Rancho" //HARDCODEADO
		}
	})
		.done(function (data) {
		})
		.fail(function (err) {
			alert("error")
			alert(err)
		});
}
function cameraError(e) {
	alert("Camera Error")

}