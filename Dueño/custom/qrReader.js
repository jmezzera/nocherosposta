function scanQR(callback) {
	cordova.plugins.barcodeScanner.scan(
		function (result) {
			/*alert("We got a barcode\n" +
				"Result: " + result.text + "\n" +
				"Format: " + result.format + "\n" +
				"Cancelled: " + result.cancelled);*/
			callback(result.text)
		},
		function (error) {
			alert("Scanning failed: " + error);
		},
		{
			preferFrontCamera: false, // iOS and Android
			showFlipCameraButton: false, // iOS and Android
			showTorchButton: true, // iOS and Android
			torchOn: false, // Android, launch with the torch switched on (if available)
			saveHistory: false, // Android, save scan history (default false)
			prompt: "Escanear QR", // Android
			resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
			formats: "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
			orientation: "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
			disableAnimations: true, // iOS
			disableSuccessBeep: false // iOS and Android
		}
	);
}