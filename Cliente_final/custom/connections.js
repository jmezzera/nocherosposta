var requestedPromos = [];
/*
function acquireBasicData() {
	//prueba();
	var httpAquireBasicData = new XMLHttpRequest();
	var respObj;
	httpAquireBasicData.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText)
			//respObj = JSON.parse(this.responseText);
			for (index in respObj){
				var bar = respObj[index];
				bars.push(bar);
			}

			//acquireFullData(); //Primero carga las cosas básicas. Una vez que termina con esto, pide el resto
		}
	}
	httpAquireBasicData.open('GET', server +'api/clubs', true);
	httpAquireBasicData.send();
	return false;
}*/
function acquireFullData(){
	var httpAquireFullData = new XMLHttpRequest();
	var respObj;
	httpAquireFullData.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			respObj = JSON.parse(this.responseText);
			for (var receivedBarIndex in respObj){
				var receivedBar = respObj[receivedBarIndex];
				for(var loadedBarsIndex in bars){
					var loadedBar = bars[loadedBarsIndex];
					if(receivedBar.id === loadedBar.id){
						receivedBar.imgUrl = loadedBar.imgUrl;
						receivedBar.lat = loadedBar.lat;
						receivedBar.lng = loadedBar.lng;
						receivedBar.name = loadedBar.name;
						bars[loadedBarsIndex]=receivedBar;
						break;
					}
				}
			}
		}
	}
	httpAquireFullData.open('GET', server +'api/fullInfo', true);
	httpAquireFullData.send();
	return false;

}
function authenticate(email, password){

	$.ajax({
		type: 'POST',
		url: ''+server + 'api/user/signin',
		data: {
			'email': email,
			'password': password
		},
		success: function(response){
			currentUser = response
			console.log(currentUser)
			mui.viewport.showPage("main-page", "DEF");
			completeUserProfile(response.username,response.picture)
			showData(response);
		},
		error: function(e){
			//mui.toast('El usuario o la contraseña ingresados son incorrectos.', 'center', 'long');
			window.plugins.toast.showWithOptions({
				message: "El usuario o la contraseña ingresados son incorrectos.",
				duration: "short", // 2000 ms
				position: "bottom",
				styling: {
				  opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
				  backgroundColor: '#F78181', // make sure you use #RRGGBB. Default #333333
				  textColor: '#8A0808', // Ditto. Default #FFFFFF
				  textSize: 18, // Default is approx. 13.
				  cornerRadius: 3, // minimum is 0 (square). iOS default 20, Android default 100
				  horizontalPadding: 20, // iOS default 16, Android default 50
				  verticalPadding: 16 // iOS default 12, Android default 30
				}
			  });
		}
		
	})
}

function promoRequestConnection(userId,promoId,clubId){
	console.log(promoId)
    $.ajax({
		type: 'POST',
		url: server + 'api/requestPromo',
		data: {
			promoId: promoId,
			'userId': userId, //HARCODEADO
			'status':'requested',
			clubId: clubId
		},
		success: function(msg, status, jqXHR){
			//var requestedPromo = promo;		
			var response = msg;
            var requestedPromo = response.promo;
            requestedPromo.QR = response.imgUrl;
			requestedPromos.push(requestedPromo);
			mui.viewport.showPage("bar_user_qrs", "SLIDE_RIGHT");
			document.getElementById("qrs_promos_containers").innerHTML += `
			<div id="qrs_promo" onclick="expandQR('${response.imgUrl}','${response.promoName}','solicitada')">
				<img src =`+response.imgUrl+`>
				<div id=promo_chars>
					<div id=promo_state>Solicitada</div>
					<div id=promo_description>`+response.promoName+`</div>
				</div>
			</div>`
		},
		error: function(e){
			console.log(e);
		}
		
	})
    
}
function getImages(){
	for(index in bars){
		getImagesFromBar(bars[index].name)
	}
}
function getImagesFromBar(barname){
	$.ajax({
		type: 'POST',
		url: server + 'api/clubs/getImages',
		data: {
			club: barname
		},
		success: function(msg, status, jqXHR){
			var images = msg.split(',')
			for(index in bars){
				var bar = bars[index];
				if (bar.name == barname){
					bar.images = images;
					break;
				}
			}			
		},
		error: function(e){
			console.log(e);
		}
		
	})

}

function getRequestedPomos(){
    return {...requestedPromos}; //Temporal, más adelante va a ser un pedido al server que devuelve un JSON
}
function expandQR(imgUrl,promoName,status){
	document.getElementById('expanded_qr').style.display = 'block';
	document.getElementById('expanded_qr').innerHTML = `
	<div id="qrs_promo_extended">
		<div id=exit_promo onclick= "document.getElementById('expanded_qr').style.display = 'none';"></div>
		<img src =`+imgUrl+`>
		<div id=promo_chars_extended>
			<div id=promo_state_extended>`+status+`</div>
			<div id=promo_description_extended>`+promoName+`</div>
		</div>
	</div>`
}


function sendTokenForPushNotification(tokenId, userName) {	
	if (mui.connectionAvailable() && mui.cordovaAvailable()) {
		try{
		$.ajax({	
			url: ''+server + 'api/pushRegister',
			type: 'POST',
			crossDomain: true,
			data: {
				tokenid: tokenId,
				devicename:device.name,
				deviceplatform:device.platform,
				devicemodel: device.model,
				deviceuuid: device.uuid,
				deviceversion: device.version,
				username: userName
			}
		})
		 .done(function(data) {
			 pushNotificationTokenId = tokenId;	//Establezco la variable global.
		 })
		 .fail(function(err) {
			mui.alert("errorr fail " +err, "Atención");
		 });
		}catch (err){
		}
	}
}
