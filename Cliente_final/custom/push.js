var androidSenderID = "275143829213";
var windowsChannelName = "windowsisdead";  
/*
 * Método principal usado para registrar el manejador de PushNotifications.
 */
function pushNotificationRegister(userName) {
	if (typeof PushNotification != "undefined") {
		try {
			var push = PushNotification.init({
			    android: {
			        senderID: androidSenderID
			    },
			    ios: {
			        alert: "true",
			        badge: "true",
			        sound: "true"
			    },
			    windows: {}
			});
			
			//Pongo en cero el badge.
			push.setApplicationIconBadgeNumber(function() {}, function() {}, 0);
			
			//Evento al registrarse el dispositivo. Envío el token al servidor.
			push.on('registration', function(data) {
				sendTokenForPushNotification(data.registrationId, userName);
			});
			
			//Evento al recibir una notificación. Se ejecutará una vez que la App sea levantada por el usuario al hacer click sobre el mensaje
			//o si está abierta en primer plano.
			push.on('notification', function(data) {
			    // data.message,
			    // data.title,
			    // data.count,
			    // data.sound,
			    // data.image,
			    // data.additionalData
				push.setApplicationIconBadgeNumber(function() {}, function() {}, 0);
				if ( data.message ) {
					try {
				    	mui.vibrate(50);
						window.plugins.toast.showLongCenter(data.message);
					} catch (err) {
						//alert("Error al recibir notificación: " + err.message);
					}
			    }
			});
			
			push.on('error', function(e) {
			    //mui.alert('Error al registrar push: ' + e.message);
			});
				
		} catch(err) {
			alert("error")
			//mui.alert("Catch: " + err.message, "Atención");
		}
	}
} //fin function pushNotificationRegister(tokenHandler)

/**
 * Esta función envía al servidor el tokenId del dispositivo para el envío de push notifications.
 * Envía además información sobre la plataforma, vesión, modelo, etc.
 * @param tokenId
 * @param userName
 */

