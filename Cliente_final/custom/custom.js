var bars = [];
var loadedPromos = [];
var currentUser;
var focusedBar;
//var server = 'https://nocheros-api.mybluemix.net/' //CLOUD del faja
var server = 'http://190.64.48.66:3000/'; //UM
//var server = 'http://localhost:3000/'; //Localhost
//var server = 'http://192.168.43.79:3000/'
//All ready!. Page &  Cordova loaded.
//Todo listo!. Página & Cordova cargados.
function deviceReady() {
	try {
		//Example when Internet connection is needed but not mandatory
		//Ejemplo de cuando se necesita conexióna a Internet pero no es obligatoria.
		if (!mui.connectionAvailable()){
			if ('plugins' in window && 'toast' in window.plugins)
				mui.toast('We recommend you connect your device to the Internet');
			else
				mui.alert('We recommend you connect your device to the Internet');
		}
		
		//Install events, clicks, resize, online/offline, etc. 
		installEvents();
		installEvents2();
		acquireBasicData();
		pushNotificationRegister(currentUser.email);  //TODO Hay que ver de donde sacar el username
		//Hide splash.
		//Ocultar el splash.

		
		if (navigator.splashscreen) {
			navigator.splashscreen.hide();
		}

	} catch (e) {
		//your decision
		//tu decisión
	}
}

/**
 * Install events, clicks, resize, online/offline, etc., on differents HTML elements.
 * Instala eventos, clicks, resize, online/offline, etc., sobre diferentes elementos HTML.
 */
function installEvents() {
	
	mui.util.installEvents([
		//Mail list click/touch events. See that if the event is not specified, click is assumed.
		{
			id: '.mui-backarrow',	//Important!
			fn: () => {
				mui.history.back();
				return false;
			}
		},
		//It's a good idea to consider what happens when the device is switched on and off the internet.
		//Es buena idea considerar que pasa cuando el dispositivo se conecta y desconecta a Internet.
		{
			id: document,
			ev: 'online',
			fn: () => {
				//Do something
			}
		},
		{
			id: document,
			ev: 'offline',
			fn: () => {
				//Do something
			}
		},
		//Typically fired when the device changes orientation.
		//Típicamente disparado cuando el dispositivo cambia de orientación.
		{
			id: window,
			ev: 'resize',
			fn: () => {
				//Do something if you need
			}
		},
		{
			id: '#userPromos',
			ev: 'click',
			fn: function(){
				console.log("HOLA")
				mui.viewport.showPage('promo_page', 'SLIDE_RIGHT');
			}
		},
		{
			id: '.options',
			ev: 'click',
			fn: function(){
				mui.screen.showPanel('option-panel', 'SLIDE_RIGHT');
			}
		},
		{
			id: "#arrow",
			ev: 'click',
			fn: () => {
				var filtersDiv = document.getElementById('filters')
				var arrow = document.getElementById('arrow')
				var gps = document.getElementById('gps')
				if(document.getElementById('arrow').style.top =='70%'){
					arrow.style.top = '95.5%';
					filtersDiv.style.top = '95.5%';
					gps.style.top = '85%';
					$('#arrow').css('transition','all 0.99s')
					$('#filters').css('transition','all 0.999s')
					$('#arrow_int').css('transition','all 0.8s')
					$('#arrow_int').css('transform','rotate(360deg)')
					$('#gps').css('transition','all 1s')
				}else{
					arrow.style.top = '70%';
					filtersDiv.style.top = '70%';
					gps.style.top = '64.5%';
					$('#arrow').attr('data-before','f0ab')
					$('#arrow').css('transition','all 0.99s')
					$('#arrow_int').css('transition','all 0.8s')
					$('#arrow_int').css('transform','rotate(180deg)')
					$('#filters').css('transition','all 0.999s')
					$('#gps').css('transition','all 1s')

				}
			}
		},
		{
			id: map,
			ev: 'click',
			fn: () => {
				var filtersDiv = document.getElementById('filters');
				var arrow = document.getElementById('arrow')
				var gps = document.getElementById('gps')
				if(document.getElementById('arrow').style.top =='70%'){
					arrow.style.top = '95.5%';
					filtersDiv.style.top = '95.5%';
					gps.style.top = '85%';
					$('#arrow').css('transition','all 0.99s')
					$('#filters').css('transition','all 0.999s')
					$('#arrow_int').css('transition','all 0.8s')
					$('#arrow_int').css('transform','rotate(360deg)')
					$('#gps').css('transition','all 1s')
				}else{
					arrow.style.top = '70%';
					filtersDiv.style.top = '70%';
					gps.style.top = '64.5%';
					$('#arrow').attr('data-before','f0ab')
					$('#arrow').css('transition','all 0.99s')
					$('#arrow_int').css('transition','all 0.8s')
					$('#arrow_int').css('transform','rotate(180deg)')
					$('#filters').css('transition','all 0.999s')
					$('#gps').css('transition','all 1s')

				}
				if (directionsDisplay.getMap())
					directionsDisplay.setMap(null);
			}
		},
		{
			id: gps,
			ev: 'click',
			fn: function(){
				centerMapOnDeviceLocation();
			}
		},
		{
			id: '.selectable',
			ev: 'click',
			fn: (e) => {
				var containerDiv = e.target.parentElement; //Div padre del que llama al evento. Es el contenedor de los tres símbolos (money_filter o distance_filter)
				var symbolsDiv = containerDiv.children; // Es una colección que tiene todos los divs de los símbolos
				for (var i = 0; i < symbolsDiv.length; i++){
					symbolsDiv[i].style.color = 'black'; // Se ponen todos en negro
				}
				e.target.style.color = 'green'; // Se pone el seleccionado en verde
			}
		},
		{
			id: '#login_btn',
			ev: 'click',
			fn: function(e){
				var email = $("#email_input").val();
				var password = $("#password_input").val();
				authenticate(email, password);
			}
		},{
			id: '#club_photos',
			ev: 'click',
			fn: function(e){
				document.getElementById('bar_icon').innerHTML =`<img src="`+currentBar.picture+`" id=bar_icon_img>`
				document.getElementById('info_bar').innerHTML = currentBar.description
				mui.viewport.showPage("bar_page_photos","DEF")
			}
		},{
			id: '#home',
			ev: 'click',
			fn: function(e){
				console.log(mui.viewport.getCurrentPageId())
				if (mui.viewport.getCurrentPageId() == "bar_page_photos")
					mui.viewport.showPage("bar_page","SLIDE_RIGHT")
				else if (mui.viewport.getCurrentPageId() == "bar_user_qrs")
					mui.viewport.showPage("bar_page","SLIDE_LEFT")
				else if(mui.viewport.getCurrentPageId() == "street_view")
					mui.viewport.showPage("bar_page","SLIDE_RIGHT")
			}
		},{
			id: '#gift',
			ev: 'click',
			fn: function(e){
				if (mui.viewport.getCurrentPageId() == "bar_page")
					mui.viewport.showPage("bar_user_qrs","SLIDE_RIGHT")
				else if(mui.viewport.getCurrentPageId() == "street_view" || mui.viewport.getCurrentPageId() == "bar_page_photos")
					mui.viewport.showPage("bar_user_qrs","SLIDE_RIGHT")
			}
		},
		// {
		// 	id: '.photo_item_img',
		// 	ev: 'click',
		// 	fn: function(e){
		// 		console.log(e)
		// 		var image = e.target.src
		// 		document.getElementById("selected_photo").src = image
		// 		mui.viewport.showPage("full_photo","DEF")
		// 	}
		// },
		{
			vp: mui.viewport,
			ev: 'swipedown',
			fn: function(e){
				mui.screen.showPage("main-page", "SLIDE_DOWN");
			}
		},
		{
			vp: mui.viewport,
			ev: 'swiperight',
			fn: function(e){
				var currentPageId = mui.viewport.getCurrentPageId();
				if(currentPageId === "bar_page"){
					//Acá va a ir la página que no está agregada todavía
				}else if(currentPageId === "bar_page_photo" ||currentPageId === "street_view"){
					mui.viewport.showPage("bar_page", "DEF");
				}
			}
		},
		{
			vp: mui.viewport,
			ev: 'swipeleft',
			fn: function(e){
				var currentPageId = mui.viewport.getCurrentPageId();
				if(currentPageId === "bar_page"){
					mui.viewport.showPage("bar_page_photos", "DEF");
				}else if(currentPageId === "bar_page_photo"){
					//Acá va a ir la página que no está agregada todavía
				}
			}
		},
		{
			id: '.back_arrow',
			ev: 'click',
			fn: function(e){
				mui.viewport.showPage('bar_page_photos',"SLIDE_RIGHT")
			}
		},
		{
			id: street_view_icon,
			ev: 'click',
			fn: function(e){
				console.log('a')
				mui.viewport.showPage('street_view',"DEF")
			}	
		},
		{
			id: '#account_btn',
			ev: 'click',
			fn: function(e){
				mui.viewport.showPage('registry-page',"SLIDE_LEFT")
			}
		},
		{
			id:'#registry_btn',
			ev: 'click',
			fn: function(){
				var email = $("#email_input_registry").val();
				var username = $("#name_input_registry").val();
				var password = $("#password_input_registry").val();
				var re_password = $("#re_password_input_registry").val();
				registry(email,username, password,re_password);
			}
		},{
			id: '#userProfile',
			ev: 'click',
			fn: function () {
				document.getElementById('option-panel').style.visibility = 'hidden'
				mui.viewport.showPage("user-profile-page",'SLIDE_LEFT')
			}
		},{
			id: '#mainPage',
			ev: 'click',
			fn: function () {
				document.getElementById('option-panel').style.visibility = 'hidden'
				console.log(mui.viewport)
				mui.viewport.showPage("main-page",'SLIDE_LEFT')
			}
		}
	]);
}
function installEvents2(){
	document.getElementById("directions").addEventListener('click', function(){
		displayRouteToPoint({ lat: focusedBar.location.lat, lng: focusedBar.location.lng })
		mui.screen.showPage("main-page", "SLIDE_DOWN")
	})
	var slider = document.getElementById("distance_slider");
	slider.oninput = function(){
		var distance = 100 * slider.value;
		if (slider.value == 100)
			distance = Infinity;
		filterDistance(distance);
	}

}

function showData(response){
	document.getElementById("user_photo_main").src = "./custom/images/"+response.picture+".jpg" //debemos sacar de la sesion o pedirselo a la base basandonos en la sesion
	document.getElementById('userSkills').innerHTML =`
	<div id=name_panel>`+response.username+`</div>
	<img src=./custom/images/`+response.picture+`.jpg id=user_photo_panel>
	`
	for (index in bars){
			var bar = bars[index];
			addMarker(bar);
		}
}
function registry(email,username,password,re_password){
	$.ajax({
		type: 'POST',
		url: ''+server + 'api/user/signup',
		data: {
			'email' : email,
			'username': username,
			'password': password,
			're_password': re_password, //confirmacion password
		},
		success: function(response){
			currentUser = response
			completeUserProfile(response.username,response.picture)
			mui.viewport.showPage("main-page", "SLIDE_LEFT");
			showData(response);
		},
		error: function(e){
			alert(e);

			console.log(e);
		}
		
	})
}
function showBarPage(bar){
	document.getElementById('bar_name').innerHTML = bar.name;
	document.getElementById('photo').innerHTML = '<img src="'+bar.picture+'" class="photo_img">';
	document.getElementById("user_photo_bar").src = "./custom/images/photo_user.jpg" //debemos sacar de la sesion o pedirselo a la base basandonos en la sesion
	document.getElementById("photo_container").innerHTML = generateBarImageHTML(bar);
	loadedPromos = []
	acquirePromos(bar.email, function(clubPromos){
		var promosHTML = '';
		for (var index in clubPromos){
			var promo = clubPromos[index];
			loadedPromos.push(promo);
			var start_time_hours =  new Date(promo.start_time).getHours()
			var start_time_minutes =  new Date(promo.start_time).getMinutes()
			var finish_time_hours =  new Date(promo.finish_time).getHours()
			var finish_time_minutes =  new Date(promo.finish_time).getMinutes()
			if (start_time_hours < 10){
				start_time_hours = '0'+start_time_hours
			}
			if (start_time_minutes < 10){
				start_time_minutes = '0'+start_time_minutes
			}
			if (finish_time_hours < 10){
				finish_time_hours = '0'+finish_time_hours
			}
			if (finish_time_hours < 10){
				finish_time_hours = '0'+finish_time_hours
			}
			promosHTML += '<div class="promo"> <div class="promo_img"> <img class="promo_icon" src="./custom/images/drink_type/'+ promo.picture+'" alt="">'+ '</div>'
			+ '<div class="promo_text">'+promo.name +'</div>'
			+ `<div class="get_promo" onclick="promoRequestConnection('${currentUser.email}',${promo._id}','${bar.email}');">Solicitar</div>`
			+ '<div id="description_time">'+start_time_hours+':'+start_time_minutes+' a '+finish_time_hours+':'+finish_time_minutes+'</div>'		
			+ '</div>';
		}
		currentBar = bar //sacar de la sesion?
		var photos = document.getElementsByClassName('photo_item_img');
		for (var index = 0; index < photos.length; index++) {
			console.log(index)
			var photo = photos[index];
			photo.addEventListener('click', function (e) {
				var image = e.target.src
				document.getElementById("selected_photo").src = image
				document.getElementById('min_icon_bar').src = bar.picture
				mui.viewport.showPage("full_photo", "DEF")
			})
		}
		document.getElementById('promos').innerHTML = promosHTML;
		mui.screen.showPage("bar_screen", "SLIDE_UP");
		mui.viewport.showPage("bar_page", "SLIDE_UP");
	})
	acquirePromoHistory(currentUser.email,bar.email,function(userRequestedPromos){ //HARCODEADO
		if(userRequestedPromos == '')
			document.getElementById("qrs_promos_containers").innerHTML = ``
		else{
			for (var index in userRequestedPromos){
				var promo = userRequestedPromos[index];
				loadedPromos.push(promo);
				document.getElementById("qrs_promos_containers").innerHTML += `
				<div id="qrs_promo" onclick="expandQR('${promo.qr_url}','${promo.promo.name}','solicitada')">
					<img src =`+promo.qr_url+`>
					<div id=promo_chars>
						<div id=promo_state>Solicitada</div>
						<div id=promo_description>`+promo.promo.name+`</div>
					</div>
				</div>`
			}
		}	
	})
}
function acquirePromos(barmanId, callback) {
	//prueba();
	var httpAcquirePromos = new XMLHttpRequest();
	var respObj;
	httpAcquirePromos.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var promos = []
			respObj = JSON.parse(this.responseText);
			var promosHTML=''
			for (index in respObj){
				var promo = respObj[index];
				promos.push(promo);
			}
			callback(promos)
		}
	}
	httpAcquirePromos.open('GET', server +'api/clubs/'+barmanId+'/promos', true); 
	httpAcquirePromos.send();
	return false;
}
function acquirePromoHistory(userId,email,callback) {
	//prueba();
	var httpAcquirePromoHistory = new XMLHttpRequest();
	var respObj;
	httpAcquirePromoHistory.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var promos = []
			respObj = JSON.parse(this.responseText);
			var promosHTML=''
			for (index in respObj){
				var promo = respObj[index];
				promos.push(promo);
			}
			callback(promos)
		}
	}
	httpAcquirePromoHistory.open('GET', server +'api/requestedPromos/'+userId+'/'+email, true); 
	httpAcquirePromoHistory.send();
	return false;
}
function completeUserProfile(userName,userPicture){
	document.getElementById('main_profile_image').innerHTML = 	`<img src ="./custom/images/`+userPicture+`.jpg" class=user_photo>`
	document.getElementById('name_description').innerHTML = ""+userName
}
function showRequestedPromos(){
	var promosHTML = '';
	for (var index in requestedPromos){
		var promo = requestedPromos[index];
		promosHTML += '<div class="promo" onclick = \'showQR("' + promo.QR + '")\'> <div class="promo_img"> <img class="promo_icon" src="./custom/images/beverageIcons/'+ promo.type +'.png" alt="">'+ '</div>'
		+ '<div class="promo_text">'+promo.description +'</div>'
		+ '<div class="description_time">'+promo.startTime + ' a ' + promo.finishTime + '</div>'
		+ '</div>';
	}
	document.getElementById('requested_promo_container').innerHTML = promosHTML;
}
function showQR(QRsrc){
	document.getElementById('QR').src=QRsrc;
	mui.screen.showPage('QR_page','SLIDE_UP');
}
function acquireBasicData() {
	//prueba();
	var httpAquireBasicData = new XMLHttpRequest();
	var respObj
	httpAquireBasicData.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			//console.log(this.responseText)
			respObj = JSON.parse(this.responseText);
			clubs = respObj.clubs;
			for (index in clubs){
				var bar = clubs[index];
				bars.push(bar);
			}
			//acquireFullData(); //Primero carga las cosas básicas. Una vez que termina con esto, pide el resto
			getImages()
		}
	}
	httpAquireBasicData.open('GET', server +'api/clubs', true);
	httpAquireBasicData.send();
	return false;
}
function generateBarImageHTML(bar){
	var images = bar.images;
	var html = '';
	for(index in images)
		html += '<div><img src="' + images[index] + '" class="photo_item_img" ></div>'
	return html;
}