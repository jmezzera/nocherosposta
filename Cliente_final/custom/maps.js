var map;
var panorama;
var positionMarker;
var directionsDisplay;
var directionsService;
var currentPositon;
var markers = [];
function initMap() {
  var mvd = { lat: -34.901112, lng: -56.164532 };
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: mvd,
    disableDefaultUI: true, // Deshabilita los controles (saca los botones)
  });
  map.addListener('click', () => {
    document.getElementById('filters').style.top = "94.3%";
  })
  positionMarker = new google.maps.Marker({
    position: { lat: 0, lng: 0 },
    map: map,
  })
  positionMarker.setIcon(({
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
    scale: 6
    }));
  navigator.geolocation.watchPosition(onSuccess, onerror);
  panorama = new google.maps.StreetViewPanorama(
    document.getElementById('street_view'), {
      pov: {
        heading: 34,
        pitch: 10
      },
      addressControl: false,
      enableCloseButton: false,
      fullscreenControl: false,
    });
}
function addMarker(bar) {
  var icon = {
    url: bar.marker, // url
    scaledSize: new google.maps.Size(75, 75), // scaled size
};

  var marker = new google.maps.Marker({
    position: { lat: bar.location.lat, lng: bar.location.lng },
    title: bar.name,
    animation: google.maps.Animation.DROP,
    icon: icon,
    map: map
  });
  marker.addListener('click', function () {
    map.panTo(marker.getPosition());
    showBarPage(bar);
    focusedBar = bar;
    panorama.setPosition({ lat: bar.location.lat, lng: bar.location.lng });
  });
  markers.push(marker);
}
var onSuccess = function (position) {
  console.log(position)
  currentPositon = position;
  positionMarker.setPosition({lat: position.coords.latitude, lng: position.coords.longitude});
  if (position.coords.heading)
    positionMarker.getIcon().rotation = position.coords.heading;
};

// onError Callback receives a PositionError object
//
function onError(error) {
  alert('code: ' + error.code + '\n' +
    'message: ' + error.message + '\n');
}
function centerMapOnDeviceLocation(){
  map.panTo(positionMarker.getPosition());
}
function displayRouteToPoint(endCoords) {



  directionsDisplay = new google.maps.DirectionsRenderer({
    map: map,
    suppressMarkers: true,
    suppressInfoWindows: true,
  });// also, constructor can get "DirectionsRendererOptions" object
  directionsDisplay.setMap(map); // map should be already initialized.

  var request = {
      origin : positionMarker.getPosition(),
      destination : endCoords,
      travelMode : google.maps.TravelMode.DRIVING
  };
  var directionsService = new google.maps.DirectionsService(); 
  directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
      }
  });
}

function filterDistance(distance){
  for(var index in markers){
    var marker = markers[index];
    if(google.maps.geometry.spherical.computeDistanceBetween(positionMarker.getPosition(), marker.getPosition()) <= distance){
      if (!marker.getMap())
        marker.setMap(map);
    }
    else{ 
      marker.setMap(null);
    }
  }

}