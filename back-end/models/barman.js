const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const barmanSchema = new Schema({
    barmanName : {type: String, require: true},
    picture : String,
    password : {type: String, require: true},
    email: {type:String, unique:true},
    usertype: {
        type: String,
        require: true, 
        category: {'enum':['owner','barman']}
    },
    clubId: String //esto va a ser el email del club, en el owner, puede conincidir con su email
});
var Barman = mongoose.model('Barman', barmanSchema);
module.exports =  Barman//permite exportar el schema para poder utilizado desde cualquier otro lugar de la aplicacion.