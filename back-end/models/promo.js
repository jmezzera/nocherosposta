
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const promoSchema = new Schema({
    name : {type: String},
    picture : String,
    start_time: Date,
    finish_time: Date,
    description : String,
    clubId: String, //esto es el email del club que en barman es clubId
    barmanId: String, //necesario para una promo para una barra - si es usuario tipo owner, devolvemos todas las promos
});

module.exports = mongoose.model('Promo',promoSchema);//permite exportar el schema para poder utilizado desde cualquier otro lugar de la aplicacion.