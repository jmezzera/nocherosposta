
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
    userId: {type:String, require: true},
    status: { 
        type: String,
        require: true, 
        category: {'enum':['request','used']}
    },
    promoId: {type:String, require: true},
    qr_url: {type:String, require: true},
    clubId: {type:String, require: true}
});

var Transaction = mongoose.model('Transaction', transactionSchema);
module.exports = Transaction//permite exportar el schema para poder utilizado desde cualquier otro lugar de la aplicacion.