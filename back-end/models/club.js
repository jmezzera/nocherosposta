
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clubSchema = new Schema({
    name : {type: String},
    email: {type: String, index:{unique:true}},
    picture : String,
    description : String,
    location: {
        lat: Number,
        lng: Number
    },
    marker: String
});

var Club = mongoose.model('Club', clubSchema);
module.exports = Club//permite exportar el schema para poder utilizado desde cualquier otro lugar de la aplicacion.