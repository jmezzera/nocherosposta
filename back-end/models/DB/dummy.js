const config = require('../../config')
exports.getBars = () =>{
    return {
        1 : {
            id: 1,
            name: "El rancho",
            lat: -34.898123,
            lng: -56.146334,
            imgUrl:  config.server  + 'static/images/ubicacionRancho.png'
        },
        2 : { 
            id: 2,
            name : "News",
            lat: -34.906512,
            lng: -56.136659,
            imgUrl: config.server + 'static/images/elrancho.png' 
        },
        3 : {
            id: 3, 
            name : "El Tropy",
            lat: -34.884363,
            lng: -56.193825,
            imgUrl: config.server  + 'static/images/eltropy.png' 
        }
    };
}

exports.authenticate = (username, password) =>{
    return username == 'Juan' && password == '1234'
}

exports.getFullInfo = () => {
    return {
        1 : {
            id: 1,
            name: 'El rancho',
            description: 'El rancho bar se encuentra en Joaquín Muñoz 3190',
            //logo: config.server + 'static/images/elRancho.jpg',
            logo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTExMVFhUXFRUXFRcWFxcVGBgYFxYWGBUWFxUYHSggGB0lHRcXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICMtNS8tLS0vLy0tLy0tLS0tKy0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAEAwUGBwABAgj/xABPEAACAQIDBAYGBQgHBgUFAAABAgMAEQQSIQUGMUEHEyJRYXEUMoGRobEjQlLB0TNUYnKCkrLSFRYkQ5Oi8Bc0U2Nz4USDo7PCCMPT4/H/xAAaAQADAQEBAQAAAAAAAAAAAAACAwQBAAUG/8QAMhEAAgECBAUBBwQCAwAAAAAAAQIAAxEEEiExEyJBUWFxFDKhscHR8EKBkeEjUgVD8f/aAAwDAQACEQMRAD8ApIGtitBKJjwTlSw4CsJAjADCMBishvYVxiZAxJsNaFse6uo9Tagya3jA00RW0valGjtXFrmw9tENZhFpyBfypYJXapXYWuJmARMJXWWlQK7C0N4WWIBK6C0sFrdhWZpuWJZK6EdK6VrrVHEiszQss56qsyVvrl7x767VhWXm2ieSuclEisKVmadaC5azJRJStCOuzTLQbJXJjosx1rqq7NNtAylayUZ1Na6mszTMsDMVcLhwDqSfhR5ipPJr7K0PMKwQxUmyUcyUk0daGmFYJlrKJ6usos0zLAxRkONYKVB0Ndrho+qzZu1fhQZTxrdGmaidFq7jTWlMKoscw4VzK/Ie7urb62EO1hmJnMhubD30rGlqyKO1LotcTaDqTczSiuwKUC1uNGZlRFZnY2VVBZmPcFGpNBeHa04sKUweElncRwRvK5+rGpc+ZA4DxOlWjul0PllE20n6tAL9SjAG3/NlGi+S+8cKedodI2z9nocPsyBJCOcYyQ37zJa8p8Re/wBqttbUwC19FkT2H0OY+YAzvHhlPI/Syfuoco/eqUr0W7JwgvjMWT/1ZkgX2BbH41Bts767Sxd8+IaND/dwfRL+8DnPtao8MHqWOpPEnUnzPE0BqqNoQpudzLijfdiDgMK1ueR8QffZqOw2+W766IYF8sJIvx6qqTGG8K7GF8Kzj+Jvs/mX7HvPsWUWM+Ct3SdWnwcCiDursnEi4w2EkB+tGsf8Udee/RBWkwYU5l7LDgy6EeRGtd7QO072Y9DLp2n0PbOkv1fXQH9CQsP3ZM3wtUM210OYyK7YaWPEDXst9DJ5C5Kk+ZWmjZe9W0cPbq8XIwH1ZT1o8u3cj2EVM9j9LriwxeGuObwH4mNz8m9laKlNoJp1F8yqNobNmw79XiInifudSt/1TwYeIJFJrHXpXBbUwG1IiitFOpHajde0PFo3GZfO3kagm9PRLa8uAY95gkb/ANuQ8PJveKx6ZtddZq1Rs2kqNYnLW6vT7RIAtS/o9GyQFJMkiMjrcMrCzA6cQaVMVTM8pCiNhgrgx05GGuGjrg060bTFSLRa+z8KcmUUPImo8j91GDAIgjR0g60a60HPETztRrAMSy1lDZJO6spuXzAv4g96XggLC4tXABY6a+VLJdAb8+XeaYT23goBe52mn08+VdxQ2863CnM8flRKChJtCtecLFS6R12i0dsbZM2MnTDwLd35n1VUes7nko/AcSKC5OkKwGs52JsafGzCDDpmc6knRUXm7t9VR8eAuau7ZWxMBu/hjiJmzzEZTIReR2P91Cn1R4dwux00PwuGwewMCWOp0zNYdZPLbRQPfYcFFz3mqX2/tmfaE5nnPhGgPZjX7K/eeJPsAIsEHmLANQ+IXvdvjitpMQ5MeHv2YFOngZD9c/Achzpmiw4HKiESl0SpHqEyxKYG0HWOuuqo1IxSwiFJNSNCwEQ1sR0b1Vdrhb0OeblgIi8KUEVF4TDSEnMEAHCzFifZlFHJg6FntNCxobDXFtR4jj7LiswuCAW12Y3OrEHgfACnwYWtRYbQ+Z+dCHNrTso3jOMGVYOhKsDdWUlWB7wRqKmm7vSPPDZMYDNHw61QBIviw4OPcfOmF4wOJoDE4lOHHyp1Oqy7RVSkrjWW5tzYOD2vAsqOpax6qdPWU/ZYcx3oeHgap/a2ypcHMYZ1sw1Vh6rryZDzHxHOtbK3gnwUwlge1z20OqSC3Bh8iNRVrQYrB7ewhX1JU1INushcjRh9pD38CNNCNLCBVHmSc1I+JUJW9ISJRO0cJLhpnw84yuh5cGB9V1PNT/rUUNLIo1JA8zSApBtKb3F4O4oaVdR7fupLG7UA0QX8eX/emmbaEh1ze4U5KZMUzgR0lsONN2KxijQG5+FAySFvWJpB1tTlp94pqnaE+lt3/AVlCVlMyCLzGFRoVObhalUXMcx9grY7Z/RHDx8aJRaWTG26DaYi0ui1pFpZUpZMICc24AC5JAAGpJOgAHMk16E6PN1F2XhGkkF53XPOVUuwAFxEgFy2XuHE38Kr/oX3Z6/Eti5BePDm0d+DTEXB/YU382U8qvGKdWLBWBKNlcA+q2VWse42ZT7RTaa6XiardJ593uxG0dpYkyvg8Usa3WGMwydhe86eu2hJ8hypvTd7G/mWJ/wn/CvRu0doRYdDJNIsaDizkKLngNeJ8KYW6QdnD/xIPkkh+S0L01J1M1KrAWUSmY928f8AmeI/w2++iE3Z2h+Zzfu2q2/9omzv+Of8Kb+Suo+kHZzMF9Itc2u0cqj2sUsPM0rhUv8Ab5RnGqj9MqpN19ofmcv+UfM0t/VXaNr+hSE92aMfN6veh9oY1IY2lkOVEF2PHTyHE+FF7KkH2t5SsW6e0bAnCOPDPFp7nomPdLaH5q378X89SyXpGzfkMKzL3yOEP7qhvnSkHSCR+WwrKO+Nw/wYL86Rw8OT70dxa4G0ise6e0Ln+zHl/eRfz0sN1tofm/8A6kX89WnszaEeIiWWI5kYaHgdDYgjkQQRRJNOGDpmJOLqSo23X2j+bf8AqRfz0BPuttbULhBxPGaH5Z6f8V0pykkwYHOnJpJshI5HIsbW99IxdMJj/wB5wLot7XilEp/ddU+dCtGhfQwzUrgXtITtPdLbC6thJCP0Hjk/yq9/hUOxeMkicxyI8bjirqUb3NrXqHdnenC49C+GlD29ZTdXT9ZDqPPgeVEbc2Fh8ZH1eIiSReWYdpT3o3FT4g1SKSjYRHHY7zyj6W51AY+w0tsreOfBzpPCcroeBvZlPrI45qfwPEVKekfc6XZkgZWaTDSG0bn1kbj1b20vYEg8wD3VD+vzaMLjx/HlWAW6RhOYby7t48LFvBs1cXg9MTEDZdM2YC8mGfz0KnxU8CaomBmN2e9wbWPI87g91Trox24NnYkOHb0eWyTq1rL9iQfqk+4t4U99Ku7kGExQxQUdViWJOl1Wa128BnHa8w1CzgqSo+8FQVYAyqGUngL+Wtc+hSHgje41MF2hBbR0HlpSM20Ivtj31OMQ/RY0oO8ihwEn2D7q59GYesrAeVSCbacY+t7gaAn2wv1VJ89KYtSof0wSqjrGj0Y+Fboz+ln+ytZTc1Tt8YvliiLRCLXEdhxouNPCks0eomo1paQWFLxIO6nvczZ4xG0cLEQSOs6xtNMsQMmvgSoHtpYOY2jDyi8ujdvApszZqh9OqiaWY972LyfG4HkKjfQltF54sa8h7b4sytzsZI008hlsPKuumDbJEaYJDrL25f8Apqeyv7TD3Ie+ojuft2XZ3WdXGjiTJmDEixXNYgj9b4CnvWVHCmTJRZ0LdTHXpexhlxkWH+pFHnI5Z5CdSO8KBb9Y99RiHBA8qPxeJfFYmSeQBWcroNRYKFAHPgOdF4fAjNmA1IsTXn16mZiZfQQKgBjd/Rh0sQNRe65r+WotSu09nqIzpyNPgwvzHzFa2phfoz5GkjMbRjMoEtfAm8afqL8hTF0hf7k473iB/wARae9mH6GP/pp/CKYekjELHgXdzZQ8V9L8ZFA09te4/uH0njJ749ZEtmYcdWunIfKu8bAMp7N/DQU24HebDCIWYkqouArX4d1tajW8W9Mkt1jJRO4aE+JI+VeQtJi09UnS8tjosxPWYK4UqBNKLG1xqL3sSOJNS2c9lv1T8qr7oMcnZ8l/zmT+COrBnF1YeB+VewosoE8p/eM8v/1ikKgIci2HDjw5mgXxjN65LA8jqKkuxd0I3RbzHgPVA7vG9HNuVCPryHyy/wAteb7VQXT6T0uG5kc3c6yCdcThLrKmpBayFfrK3NlPC3tHCvS+yscs8Mcy8HUMB3XGo9h09lURLuhlF0eQEcyB8xarW6Mz/YVW98kki8SfrX4n9an4avxGNjpJcQgUAx23p2KmNwsuHcaOhAPNWGqMPEMAa8+YbdmJhYlweB1HH3V6YrzXiN4vpZeqiLfSyWOp+u1joKPFB7AqbQMOdSDOTu91YJVyRaxUjX2EVP8AYQG19kT4CQ3ngUCMtx7N2w7381KHwB76rs4/Fyeqir7yfdc06biYzEYLaUEkzHJM3o7i1h9J6h9jheXfSqDENzMI2svLtK3YEaG4I0I5gjiKzXkKsbfzdVItp4jUKsjCZR4SC7afr56avQMOnFr+Wnypj4lVOWAqXF5D+oY8ia2uCc8B9/yqWo0d7RxAnvP4mtSiS3FF+JoRXY7CcUA3kX/ouTuPuNZUh6tv+J8Kyt4r+J2Ve3zjQmIXSjYcSveKY0NERtTGpAzRUIkiimX7Q+FT7oZw4fGzScergy+2R1sfch99VVE9Wx0GkJHtCb7Kxf5Flb76CnSAe8Ko90Matv4z0nH4iUm4EhjT9WPsC3nYn21ziECrfwqL7HclQTqTqT3k6k064prISTYW76kqC73lSaJYS059yMMMKZYesMnVZ1YsTc5cwBXhrw4c6j2ynVgDflVl7vPmwuHPfBEf8i1Csd0XnrWfD46WJGJIjKLIFub2U3By9w+NWVcOGAyiQJWIJDGJDEQ2B6xLXGuYd9R/ezemKNSkYDtbU3so8L86cZOh2Rjc7Qued4P/ANlcTdCubjjj/gj+eljDkGN4qdTLP2HJmw0Dd8MR96KajnSzhWl2ZKiC7F4LDhwmQ86k+zcJ1MMcQJIjjRATxOVQtz7qjXStMybMmZDZg0Nj/wCclWNfKbSRfeEqDZu7mJUhnhkKc+rZc1vDWnSb0KEASYaVSb26xSeHm1qasPtjHldJJAAOORQPE3Iombd7aGL7UgdgOBkJt5qDpXkspZv8jADwTPUDWGg/mWd0STRPg5DCuVPSHFrAa5I76Cpuag/RFs18Pg5I3tf0hzp4pH+FTg16lK2QW2nmVPeM8lbMLZRluNBYLx9/GpHg8djtBHJKf2r2tyIapTsPAhFA9ENwPrq7a/qgEUTsiHEAzBYMQoMrEBIWQEaWsWXQVA71G2T+ZaGTv+fGRjF7Ox8i3llcDxJA92gq3OjDZRw2zokY3LF5D+2xK/5bUx7H3JlmfPi84jvfIz3ZvAhNFHfz8uNTTbO28Pg4w0zqgtZF0zNbkq/6Ap+GWoozVIis4blWI727WGGwzvqXIyRgcS7aC3lqfIGqlwOz+rVQIlHi7Xt5ilMXvQ2PmaSS6ojssUaAtZQbZiRxY21Ps5Uu0x+pBIf1gF/iNTYpw7W7RlJCgv3jdjMXlBvIFsbWRDbjbjTNvDZOpcM5IniYFj3NcaUTvA3YZXUA5l7NwdS3DTzvQu1Ii/oymVGzTYcBVH2nUcfbS8OozA26w3FhJ90yYQCXDTZQcySIxP6BVlH+d6ribKL6C9vmeFWv02Rj0XDsb6YgDQ5fWik5+wVU0ssIF+qLagXLX1PDjVGJHPAoGyxIYpLm7ADW1vMWpKXFqfVDN5Amty4kjOEiUFLd5vc20sKFfESlk1AVgCxCgZb301v3Utb9prMIv6T/AMp/cKym7rpvtn3L+FZTdfHxi8wg7bNmPBPcV/GupMI6Ldltram0S0os1WZX/P8A2BnWOES30zBfE3+4GrW6ForYTaaZwxKqdL84pAOI8Kp5cRVqf/T5iwcTjIT/AHkEbW7xG7Kf/dHvrQpBvMdgRITsa6IoLq3C1iTpbyovauGaZQBIqqORzanv0FDQbMETvEZ4w0btGQxsboxU391OCYO40nhP7VRMQGvf4SxdVsZ6C3OH9gwmt/7NDr/5a1D94uliOCd4YYDN1bFGcydWuZTZgtlYmxBF9OFG7vb54ODARI8ymSLDqrKoJJZEsQCBY8KpLDi9yx1ZiT5kkk/GqHq2UWMmpUczG4lj/wC2HEFwi4KK5BOs7cr8+r8KcH6R8WFYmDDKVW+XrWZu/wBVRVYYYXxKgAfk3Gp01R9SasTFYg+jOBEgJjN3CnXs96JY+00hq7AgX3HaNNFRew6y0tlYrroYpSLGSNHt3ZlBt8abN90zYOQZQ2seh4H6RaM3cJ9Ew9+PURX/AHFoTfVwuDlJFwDHf/ESq31pn0ki+/8AvK/xu1iuHkQZFBjdbLkHIjiLk++k5Z2e4LswGmuY2sOFzoOFOT+j+iyZYwWEchveJTqrc82a2vCnRdvKE0w8amw1IJPDj2U++vn6uHpAc1T6z26dbKLol4r0Wn6Gf/r/APwWprUV3AxvWpO2mk1tAVHqLybWpVXu4YAUlAN541ck1CSJGxv5s83tiAbGxsrn5LRext6cHimKQTq7rxXtK2nGysAT7KrTYey42ypkQXEjC+vqEDXXncVHm2TKk2IkilETwyo6MDYZuwQPcTpbUXFSpjHLEMLCP9nQjQ6z0LVQdJexGw+KGKGUxzsAzuMxjcKezcn1TbQedWNurt5MZAHUjOvZlUfVcAXHkeI8DR21tnJiIXhkF1cWPeDxVh3EGxHlVdVBVp2kyko0ovYmOMUOcWLs8hYLxuXIt4DnTNtbacrMxKSZFcEa2ubnXhqLj40TtvaGMwGIbBN1aFSSpVNHViSsov3638QRypj2xtKZluzk3YDgOQ4cKiWgQ/MN5eGumYdJrEYlJHRnjcWbt6mxH+re6nqTCn0jDFNUGKwwHD/ip3H7qinp0nfTpuSGn2ngkP5xGx05Ic5/hpww5DKeg8wXqpkNibnwJcvTg9sFF2cxOJWw7z1UtVPhQWhz5StyAVA4G4GbUctDerH6d9oFFwcS8Wkkc+ARVX/7lVJJjnymzNa/C4GUac/fQ4pCzaRdFSUjhNhD2vWJUXUlvW529/zpE4VQRcCxF2158vmabJHku1zfja7ePG3toUuSBrpbTU8bG/xFKWi1t4XBA3jnY96e+spnyN4VlO4PmBwl7GZtHCrGVAvqL6+dIRoDpR3W53XPlYWPIjnbv1qVbMweGylnjSwF7kU0VSoCtuZhQakbSJxYG6g3FzwFx/oVLeiLEej7UhYnKHzQOD/zB2R551j99A4bCCR82ih3zWvoO4W8uNM+1MVIsxdWIZXVlN/rLlKn3isSqzMVjHpqFvJ901boGDFHGxgmLEEZ7fUmtr7HAuPEN4VABCRfMjiw10tp36ivT2Ckg2ts5GdQ0eIiBZeatzAPJlcaHkVqh94dg4rCTHDSlALnK5LfSRn1WW97eIHA3plRiuvSJpZTod4zYGdFUXHBGA05sDa/voqKcWsL+P4UQu7LBLmWMcPtfhTLP9GzJcGxtccPZSBkqk5TKQWQaiOez5A2JTkCr352BR76eVTzG7UY4ZlDtYIygBcvAEfWJ7qq7DYzq5FfjYH4gj76d8fty6adWoN9A7Erm4ki3jS69FyVyzaboQc09I7tf7phr/8AAi/gWht9Evgph4L/ABrS+6wPoWFzet6PDflr1a30NC7+Bv6PxRT1hEzD9mzH4A1aVvTyntIAee/mQJMDeCZ1Ga8RuVC2HZbjb76e32VLlAyfV4ltOHO1VJsTbU0URjiOVXzF7E2OmW9gR3Hl3U4vvLjctuvewFvrfzV4NT/jUY69+4H0M9YV3I0P58JbXRth2SOcMLHr/wD4JUwqv+hiSV8JM8pJLYhrX00Ecf33qwK92gmSmFnlVjdyZWWx9k+td7asOXJjpUfxOzlZsYCwOWRPrgE3WP2kacqi2MbEDEToGcWxEy6BeUjDu7qSkwE175pLm1yDa/de1ea1JQTsD6/1PRplhY7/ALf3JDu3tGTZ+K65SGjfszrnvmTkw1tmBuRp3jnV7YadZEV0YMrAMpHAgi4IrzWYEsMxxBJBt2zxA86m3RBvJJA/oOIzdW5Jw7trlc6mIt3NxHjccxVOFqEcrGKxaBjmUSY9JW5g2jAClhiIrmJuGYH1omPc3I8jY99/Ou0Ga+V1KshKsraEMNCCORuDXruqt6Xejs4oHGYRf7Qo+kjH98oHEf8AMAHtAtyFWFASDJEqlVK9DKjmwq5EJGUlTfx0FjUv6Ftkq+0utsSIIne54Bn+jUedi3uqtTimIsxPZ0APLXUW5a1efQbgVw2zp8dMcolZmLHlDACL/vdYfdSadJlOpluIxNNqdgusYumLaKS7QEZcAQRKpFx6zfSH4FPdUBE0DBgHsDxvYa9+vGhdo4tsXPLinteaV3sSLgFjlX2Cw9lJYeESSdq/rMPcNKXUpi5YkxdOoVUKBD2aHQ9ZrbXUa0gzQiwzcD3/AA0FEjZUYIIvfjxrWJwwXWpw6bAmNJbrB+tg/wBXrK56sdwrKZp3PwgXinoru9i47ABHLjf8KUwuHeTMDKbK1iLmxt4Cu8O30j+SffXGyH1l/X/Gll2ynwBGBQSPMITYIHBh7Lj76R2fEJHKqWBF7kk62Nu+naNqZdhn6Zv2v4hQU6jsjknUCGVAZRbeWx0Oba6iRtnyvfrC0uHv9oAdbGL947YHg9WZtvYkGLj6vERh14i9wynvRhqp8Qa80bcxLxSQSxMVkR8yMOTAgj/+Vf8AsDeNtoYAzYVo1xOXKVftokwAJVgCDlPI9xB8Kuwz56QLSHEJkc2jLN0SYUm64jFKPs50Yf5kPxrj/ZDhec8581g//FUTbpXx8btFP6PHKjFXQxOCCDw9c3++uIelHaTCM3w/bkK6RHgL2td/CuLUhuPhNVKrbGTEdD+A4l5j/hD5R0VheifZqMGMbyZTfK7XU271UC48DpUGxPSTtIJMweEFDYfRD7Knv8aJxG/u0REziVLhM35Je69B7TRFv4+X3hezVjeXQK5ljDKVYAqwIIOoIIsQRVF4/pD2muHjkE6Atlv9EnME91O39bdoEf7xy5Rx/wAtY+OpILm+5H8TFwVRjYSQ43oqwxv1EjQ8bAgSKNb2GoNr+NJYLozdey+NOS9yqRZT72dvlUS3f302lNGzNiiSGI/JxDkO5KchvLjz/wCJb92P+Wp6uMwyOVZTcfnePTDYhluDpLV2Vs5MPEsUd8q8ybkk6lmPMmi686Y/ffaayzKMbIAoNrLFpqv6HjSuG372iIUviZJJXYhRZNfHRau46hQQN/teS+zsSby3Nt7hYXEyNN245GN2ZDox7ypBF9BwtUP3u3QfA4aTEpMsix5SUeMqTmdV0YPb63dUMO+e2Ot6o4uzEgcFsL8BfLR2J2htWVck2KR0JBKsNDY3F7AX11qbENh92tcjTWPopXBsNhGqLb5I/Ix6eLfjQuK2iWBtGBc30ZuOmo104UttySeNAZPR2F9BkPG1b2JJPIpyNCgB4dX/AN6mAQU+ILW9T+fCW5xfLlMsLcbpUQgQY89W40Wc+ow5dYfqN+lwPhVpQTK6hkYMp1DKQwI7wRoa86zYSc8ZYvZF/wB6bpnxWGBaHFMh7ogULHkLKdTVNLHKxC6fH7SKtheqi0u7eno12fj2MkkRjlb1pIjkZj3sLFWPiReox0s7TjwOAh2ZB9dFUi+ogjsO0e92FvGz08bjRYrA4KTF7VxMrOyhurkbN1SD1VA5yMSL+wd96m268uNxEmJklZS57KgCyINEQX7h7zc86prVlpjU7xFFCzdwIwxIi2IjS/kT99cY3G5QCFUG5NwOenH2U4YiHqSGJzoSA2YC4vwIIFKYyGPIewvuqLjC4J1BlxAIsBYyOnab9/lSUu0HPE0d1S/ZX3Um6KD6o91UBk/1iSrd43ekHwrKcMg7h7q1TOKvaDwz3jhhT23/AGflXGym7Uv69C7OJsRqWNrDvt40XheyzZuzcLa/lrUzrYMPT4WjUYXB9frHeI00bGP0zftfxCnONqadin6Zv2v4hSaQ5H9I1zzr6wveM6x/tfdRm5+9b7NxLYlQWjJRJohwdO1qP01sSD4kcCabtvN2o/2vuoALmVh45vcPxYVXhhakv51k1fVyPzaXzvrutDtjDpj8CyGfIShsLTKBbqpL+q4NwCeBuD4U22JmiCq4COJShjMaKY2F76Ecfxp33B3zm2cVyAyQvKVlivx0BzoT6rgew8DyItXb27uB29AuIw8iiUerKBqGH93PHx0945acWsoeJDGnKfWfNDKW7QLLmtZTqqcLC172FKSbfQqY2hksVy6ML2tbuofebY+J2ejw4mMoWkGVxqjgDijjjw4aEcwKb5jeEsND1i8O6zafKphQX9Y66fCVGs36T0+8dsTKssCR5kjylcocuTYAjtZUtfhwp+/pGLLcTR2A11N+HcReoHNLmVb8lH3/AIUZitmrCyAnNmF+FrcNOOtBVwlNrKxtqSB84VPE1FuyjsD9JIt1VKxuLg2k+rqCCo4e+ntS3ceHcaheyVvh5TnZcjlrqbX7PA1sQsbZpZdYes9Y6Hu8qkr4VXqsS3Xt4ldHElaagL07zW1oZOunOR7EHXK1uXO1KbvTL1sd/qxtbzLW+VNn9ISMpN9FUc24nvF6L2TjmSYGJQT1IABXuA5AjmONejUpNwSh7W+EgSoOIGHf6wrESf26/wCmn3VJzNrUek3rfNbLEdbXAYa3I5ny99L4Pa0sshV0RQvrWWxvyFyfbXm4nD1GVSQBlHeW0K6BiBrczje2S8S/rfdXG7k9gw8j8LUhvZMOrUfpfdRu4W7mLxtzDHaM2BmfsxixN7Hi58B7bU6lRL4TKO/1i6lQJiLntCJ8SdFUEsdFAFySeAAHE1ZO4m5XUAYvGhRKBmRGIywga53PDPb2L5608bvbpYTZqGZ2DSKt3nksMo55RwQfHxNVz0kb6yY9Hw+GzLh+BPqtNrz+yn6PPn3U+hh0w4zPvJqtZq/Km0D3134/pLEmKEn0WL1eXWvwMh8OSjxJ56MrtamTY2HaKQq4sStxw7x3U6ytSMXzVfEfhlskE2y30Lf650nLJeG/gv3Vm1T9E3lQ6PeAfqj7qJF5B6zG98+kCzHMe6w+dJudfdXZod27VVgXiTCL1lD9ZWV2UzM0Hilfk3uvSj4h9MzMfO40pGFSL+XH2iu5VPZ56feap0vJdbQzATyAsUufDiKLSJ4vpBcluAy346nQHSgsBM6ggBO136H30+4CFlXUq1tQMwvrxuTpU1U2J0H3j6Z0jTiZnksWU9m/BSPO9ahkCgqdbra44akfyinTFYpWVkCWYggEEEXPlQ/o30Mp1GotYcltb41yuMtiLTWBJve8DweIIy2sPpL+2xH30du9vDicDIZcK+Vs4zKdUdbN2XXmNPMciKBhhK2voRY3AuDm4A93CsV2e65c2Ua6ngNL2v40/S9xFG9rGX5u90hbP2onouMRI5GABimsYpDYfk3Ol9eBs3dfjTXvP0NqVY4CbqySGEUpZk0FrLILsvtDeyqSSJX9Ykadw9nyqYbr7/4/BKqpIJYwfyc5ZwB3KR2l99vCiJGxgBSLlYz7c3WxuCB9Iw0igD11HWR6c+sS4Htsa72zigyxEHhb4qPwq3th9NOCl7OKSTDtwJt1sR5aMoze9RT/AC7F2PtIZgmFmv8AWjYK/vjIYGgemGZW7fWElYqrKRvPPmHmAw8q31eRV94Bpw9IVpGtdQICvaFjpVt43oY2c4sjYiIXzWSQML2t/eKx+NAP0H4bliprdzKje24tS3w97nv/AF9o1MTYD87/AHlKxlMpAY3K6ggC1u43+6i9hygSZjyiarZi6CcODrjJz5LGPmDTpgOhfZ8Zuz4mTS3alCC3d9Gq/OnVKeZSveJp1grA9p5+klsSf9calmxdm4rE4hvRoJJA6rdgLICAOMhso99XdFufsfArnaDDoF1zzsHt45pibUDtrpV2dhgFjZpybBRCvY8PpDZbeV/KhqU1f3vT5fabTqsvuj81+8A3f6KI+zJjyJmBuIVJ6oH9M6GTy0HgakW8++uC2YgjYgyWCx4eEDNrot1Gka+Jtw0vwqp99uk7HzJliIwyE2IjN5CLc5SLj9kCoBgdXjZjclyzMxuSQCbknjrWIyrTum01kdqn+TeSLfje3G7QkHWuI4rsUhjJyrl1zMdC7eJ9gFR4QydV1nWm1r2ub8bd9LbblBKlSDbS49tbgf8Asx9vzpZdsisep+8cKah2UdBE9jzdvn6p8aPxE5pp2Oe2fI05ut6TiAOLHUCTTiWOmvGw8DSOHk+gH6tZiR2W/VP30hhG+i9h++uVRk/eYTz/ALRNn0NCyHtewUQTQs3GqUEQ50m89ZSdqyjtAuYrhsUqnUA++l5ccrD1Be3Hn91NYrdFwwTeJ4hhgn8PiKXfE5dLa/Km2lXbUmtKCErmEnFN328q00zfaPvNIhq6LV2UdoV/M7DnvPvNLw4t11BPcb66cxSceHLC+lvP8KyaErYkixva3hxrjlOk7UawlY+2bi4QAHkbWNqKbBAKM0trhfqn6wuNb/6tR0UGVZcyli9yDbw0FaWJZBErEqVyh2sWy2B5DjyqU1rmO4YAjO+UNpqOGltdeOoNq3LIGtq2nDtAWt3EKKmuF6Meuwhxq7QiEADMzNDKCuViGzLx0NN8e7OExByQ7UwwkNgOsjngRieAzOlqosNIjNvG/Yu9GLhYAY3EKtuHWuy+GhJAqQ4ff7aXWWXGuVy6XSJu7mUqO7wbq4zZzhMQmUNojp9IkluIUgcu4gGmuZgLaG/h2NbnlQupJ0MNGXLqBJE/SXtckj01ufCOEc/1K4G920JjIJMbiCAhNg5QcbfUtUZw4ub6ceFK4SYAtfiVsPeDRPcggQadrgmJTXeQM7M7EXuxLHnzOtOUq3wynmpHwNqAh1kTyUfCnHCm+HYHlm/GlVjbKfI+sbRHvDwZm25i0akjQkEe0UFCEuBJfKF5d5t3V3ipM2HjHc1vdeltlnVza+oHuBoByUj4J+cNueoPIEG2ksYA6u1tL279e+h4lJXUvl14aj20TtewIsLXtw051zC30DeZpit/jB8xbLeoR4nGzTaSwN7g+FOIk1NMmEexU+Pz0pylOp8hQV0u0ZQflis57J8qCwTfRH2/fSrNpQuEbsH20Kryn1E1m5h6GZehpG1rvrKQc61Qo1iGOk3mrKTsa3R2i7xSGTsFbca5itqD4a91JKa2K20C8KWIeNcMmtbifvrTUIveHpadZK6CUnelYIixAHE1t7Totl7K+bfdRGI9SLyf5ill2eQlmZQQSeNxY2/ClZcC2VSCpCqbm/jfSkcRbjWNCm0KwjqSM5YC3LWinmhF8oJYcTY+/uvTMs9tTYClIpOPjxpDU+sbmlu7Acf1axZFwP7Rx8xVS4iOJlN9CeY09/fVt7n43qt3MTKER8pxByyKHRrEaMh0YeFV9srpJaCRZBgNnGx1yYcRP+y6+qfYaoNMkLYxCuAWlhbzK8e7UC4on0jLB1dx282e6Cx1uIrg+2qq3d2TPjp3jjjDkLmdmOSONR9d2Pqj41aG9+zV2vs8bVwssodI3YwyNmQBL9aiqfUYZTqNGsNNQRXm428+OAxOEwuHWd8XFkOnaQAMpe4sMtnPraXt5FhUk6wA4C6TrAbgyYgSnB4nC4l47kxRSOHt4CRFDed7UwbA2LicVN1WHiZ3AAYXChb6dosQBqDpx0qy+h3ZUWH2llOLjlxHUSq0UAMkai6E5sQbKSCBoobzqBTm22Wty2kbeH9qo4N49w9FmP8AShhvog4iSRpA7dWmYuFQnLdn7DGyjlUf2ngZ8BiJcJMELoRmANwQyhgVPMEEcqnvTpjn/pGJOsdBHAkkZUlcrs8gL5l1zWRRflaqwlEkssjs7SOTcs7F2bxLHU8KBspuDDQsLETkS9nIRwfN8KK2S+jeJH303yAqSDppWkkHJTfvzW+ArHTMpA6wlqZWBPSG7XOo8vvpKFvon8z91Dzylhr7K6hP0bCx92nLnQhLIB5hF7uT4gyHQ07dYCL94FNK6XvREeqA8xRVVvBotaFysb+FC4Q9k+2lpG0oXCnRvbS1HKYxjzCaBpFzrXQakydacBEsZ1esrVZXTomK2K0tdIl6OJnSNSgPKkHFjalQvZv42rDCUxa1dRylTcG3kbUI5rQNZlvvNL2jl6e1rZzW32kSLZmI5im29ZXcJZ3EMMkmzXPhb40rBNIxCIpZjYKqqWYnuAHE0Ih7J8vvFJ3rsohFiNZ6D3W2Dif6tzYdomE8yzlImsjdtuyCGIy3tfW3Gqqh6Ndqs2X0Rl11Z3jVR4li3DyqKRwluHvvb50p6GebKPaT8q24ECxl2bQ2zh9ibGOAWeOfGSJIGEbZgrS3DsSPVCg2F7FiBoLmzb0M4RMRszaOFidUxkoZQSbN1ZjATUa5c2cG3DNVSiFBxf3CsRlUhlL3HAg5SPaNa687LLd6JN1pNn7UUYuWKOZopVjw6uJJCNCXbLogsulzc66aGo/tTdOddtALaV2xxlZIrv1URnDo8zDSO4JNjwA14ioKcawNxoeOa/av35uN/GkGckG51JJJ7z499cCes7KO8uDp22XOcamJVGMJw6IXAJUMryXDEer662vxvVXWUHS4PhpThhh2R5D5UpMinjx9lSGtzSladhGXFNdlHHhw5ilZERuGh91J9WWlIHK9dS/pU6+0DvBsRGV53pEsTxJpXEnhqfbWQuBcHnTAdIsjmtEa6jmK13KF4ih63cQTdTpChiQa4hbjQ9avWZBN4hvrFb1wTXNZRWglp1esrmsrrTLmbWlIedZWVh2mrvOZeNLp+TPnWVlYdhNXcxB60K3WUQgneaFdCt1lbMnacD5feKdD+QXyX51uspTx6waXhQT8aysrUnPNCt1qso+sXOhyrb/jWVlcd5oj9F6q+QrqPnW6yvNPWXDaN2C/LP7fnSeN9b31lZVX/Z+0n/TApuVblrKyndoo9YlWo+NbrKKBNNXFZWVwnNMrKysrYMysrKyunT//2Q==',
            promos: {
                1: {
                    promoId: 1231,
                    type: 1,
                    description: 'Corona 2x1',
                    startTime: '22:30',
                    finishTime: '3:30'
                },
                2: {
                    promoId: 1232,
                    type: 1,
                    description: 'Stella 20%',
                    startTime: '22:30',
                    finishTime: '2:30'
                },
                3: {
                    promoId: 1432,
                    type: 2,
                    description: 'Red Label 15%',
                    startTime: '23:30',
                    finishTime: '2:30'
                },
                4: {
                    promoId: 1434,
                    type: 3,
                    description: 'Daiqui 3X2',
                    startTime: '01:30',
                    finishTime: '2:30'
                }
            }
        },
        2 : {
            id: 2,
            name: 'News',
            description: 'Descripcion para news',
            logo: config.server + 'static/images/News.jpg',
            promos: {
                1: {
                    promoId: 1201,
                    type: 1,
                    description: 'Corona 3x2',
                    startTime: '22:30',
                    finishTime: '03:30'
                },
                2: {
                    promoId: 12322,
                    type: 1,
                    description: 'Patagonia de regalo',
                    startTime: '22:00',
                    finishTime: '22:30'
                },
                3: {
                    promoId: 14232,
                    type: 3,
                    description: 'Tequila 5x3',
                    startTime: '04:00',
                    finishTime: '04:30'
                }
            }
        },
        3 : {
            id: 3,
            name: 'El Tropy',
            description: 'Descripcion para el tropy',
            logo: config.server + 'static/images/elTropy.jpg',
            promos: {
            }
        }
    };
}
