var mongoose = require('mongoose');
var Transaction = require('../transaction')
module.exports = {
    requireTransactions: function(userId,clubId,callback){

        Transaction.find({
            userId:userId ,
            clubId: clubId
        },function(err,transactions){
            if (typeof callback === "function")
                callback(transactions);
        })
    },
    updatePromoStatus: function(){

    },
    createTransaction: function(req,res){

        var userId = req.body.userId
        var status = req.body.status
        var promoId = req.body.promoId
        var qr_url = req.body.qr_url
        var clubId = req.body.clubId
        console.log(clubId)
        var newTransaction = Transaction({
            status: status,
            userId: userId,
            promoId: promoId,
            qr_url: qr_url,
            clubId: clubId
        })
        var response = {
            status: 0,
            userId: userId,
            token: '1234abc',
            message: 'Te has logueado correctamente',
            imgUrl: qr_url,
            promo: promoId, //preguntar juanchi si array va a guardar las promos solicitadas, creo que debería ser base
            promoName: req.body.promoName
        }
        console.log(newTransaction)
        newTransaction.save(function (err, transaction) {
            if (err)
                return res.status(500).send({ message: err })
            res.status(200).send(response)
        })

    }

}
