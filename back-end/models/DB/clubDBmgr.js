var mongoose = require('mongoose');
//const DB = require('./dummy')
const Club = require('../club')
module.exports = {
    createClub: function(req,res,callback){
        var name = req.body.name
        var picture = req.body.picture
        var promos = []
        var description = req.body.description
        var lat = req.body.lat
        var lng = req.body.lng
        var email = req.body.email
        var marker = req.body.marker
        req.session.name = name
        var newClub = Club({
            email: email,
            name: name,
            picture: picture,
            promos: promos,
            description: description,
            location: {
                lat: lat,
                lng: lng
            },
            marker: marker
        }) 
        newClub.save(function (err, club) {
            if (err)
                return res.status(500).send({ message: err })
            callback(email)
        })
    },
    getClubsFromDB: function(req,res){
        Club.find({},{_id:0}, (err, clubs) =>{
        if (err) 
            return res.status(500).send({message: `Error al realizar la busqueda ${err}` })
        if (!clubs)
            return res.status(400).send({message: `No hay clubes`})
        return res.status(200).send({clubs: clubs})
   
      })
    },
    /* getFullInfo: function(){
        return DB.getFullInfo();
    },*/
    uploadImage: function(){
    },
    uploadPromo: function(req,res,id){
        var newPromo = id
        var clubId = req.body.clubId
        Club.findOneAndUpdate(
            {
                name: clubId,
            },//filtro
            {   $push:{
                'promos': {id: newPromo}
            }
            },function (err, promo) {
                if (err)
                    return res.status(500).send({ message: err })
                res.status(200).send("Promo agregada a bar")
            }
        )
    },
    readQRCode: function(){

    },
    updateClubDescription: function(){

    },
    updateLocation: function(){

    },
    getPromos: function(){

    },
    getImages: function(){
        
    }

}