var mongoose = require('mongoose');
var Promo = require('../promo')
module.exports = {
    addPromo: function (req, res) {
        var promoName = req.body.promoName
        var start_time = new Date()
        start_time.setHours(req.body.init_hour,req.body.init_minutes)
        var finish_time = new Date()
        finish_time.setHours(req.body.final_hour,req.body.final_minutes)
        var description = req.body.description
        var picture = req.body.picture.toLowerCase()
        if(picture.includes("whisky"))
            picture = "whisky.png"
        else if(picture.includes("cerveza"))
            picture = "beer.png"
        else
            picture = "def.png"
        var clubId = req.body.clubId
        var barmanId = req.body.barmanId
        var newPromo = Promo({
            name: promoName,
            picture: picture,
            start_time: start_time,
            finish_time: finish_time,
            description: description,
            clubId: clubId,
            barmanId: barmanId,
        })
        var responseJSON = {
            status: 0,
            message: 'Promo creada',
        }
        newPromo.save(function (err, promo) {
            if (err)
                return res.status(500).send({ message: err })
            res.send(responseJSON)
        })
    },
    getPromos: function(req,res){
        Promo.find({clubId:req.params.clubId},function(err,promos){
            res.send(promos)
        })
    },
    getPromosBarman: function(req,res){
        Promo.find({
                        clubId: req.params.clubId,
                        barmanId: req.params.barmanId
                    },function(err,promos){
                        console.log(promos)
                         res.send(promos)
        })
    },
    getPromos2: function(callback){
        Promo.find({},function(err, promos){
            if (typeof callback === "function")
                callback(promos);
        })
    }
}

