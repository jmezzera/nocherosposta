var mongoose = require('mongoose');
var User = require('../user')
//var dummy = require('./dummy')
module.exports = {

    authenticate: function (req, res) {
        var email = req.body.email
        var password = req.body.password
        var responseJSON = {
            status: 0,
            email: email,
            token: '1234abc',
            message: 'Te has logueado correctamente'
        }
        var userLogin = {
            "email": email,
            "password": password,
        }
        User.find(userLogin, (err, user) => {
            if (err)
                return res.status(500).send({ message: err })
            if (!user.length)
                return res.status(404).send({ message: 'No existe el usuario' })
            req.session.email = userLogin.email
            req.session.username = user[0].username
            req.session.picture = user[0].picture
            responseJSON.picture = req.session.picture
            responseJSON.username = req.session.username
            res.status(200).send(responseJSON)
        })
    },
    createUser: function (req, res) {
        var email = req.body.email
        var username = req.body.username
        var password = req.body.password
        var re_password = req.body.re_password
        var usertype = req.body.usertype
        if (password != re_password) {
            //CONTRASEÑAS NO COINCIDEN - DECIDIR - VER SI VA ACÁ
        }
        var newUser = User({
            email: email.toLowerCase(),
            username: username,
            picture: "photo_user",
            password: password,
            usertype: usertype
        })
        var responseJSON = {
            status: 0,
            username: username,
            token: '1234abc',
            message: 'Te has logueado correctamente'
        }
        newUser.save(function (err, user) {
            if (err)
                return res.status(500).send({ message: err })
            req.session.email = newUser.email
            req.session.username = newUser.username
            req.session.picture = newUser.picture
            responseJSON.picture = req.session.picture
            responseJSON.username = req.session.username
            responseJSON.email = req.session.email
            console.log(req.session)
            res.status(200).send(responseJSON)
        })
    },

    updateProfileImage: function () {

    },
    getProfileImage: function () {

    },
    updatePassword: function () {
    }
}