var mongoose = require('mongoose');
var Barman = require('../barman')
module.exports = {
    createFirstUser: function(req,res,email){
        var barmanName = req.body.name
        var password = req.body.email //la idea es modificarla luego
        var re_password = req.body.email
        var usertype = 'owner'
        var clubId = req.body.email
        var newBarman = Barman({
            email: email,
            barmanName: barmanName,
            picture: req.body.picture,
            password: password,
            usertype: usertype,
            clubId: clubId
        })
        var responseJSON={
            message: "barman y club creados"
        }
        newBarman.save(function (err, barman) {
            if (err)
                return res.status(500).send({ message: err })
            res.status(200).send(responseJSON)
        })
    },
    authenticate: function (req, res) {
        var email = req.body.email
        var password = req.body.password
        var responseJSON = {
            status: 0,
            email: email,
            token: '1234abc',
            message: 'Te has logueado correctamente',
        }
        var barmanLogin = {
            "email": email,
            "password": password,
        }
        Barman.find(barmanLogin, (err,barman) => {
            if (err)
                return res.status(500).send({ message: err })
            if (!barman.length)
                return res.status(404).send({ message: 'No existe el usuario' })
            responseJSON.email = barman[0].email
            responseJSON.usertype = barman[0].usertype
            responseJSON.picture = barman[0].picture
            responseJSON.clubId = barman[0].clubId
            responseJSON.barmanName = barman[0].barmanName
            req.session.email = email
            req.session.clubId = responseJSON.clubId
            req.session.usertype = responseJSON.usertype
            req.session.picture = responseJSON.picture
            req.session.name = responseJSON.barmanName
            res.status(200).send(responseJSON)
        })
    },
    createUser: function (req, res) {
        var email = req.body.email
        var barmanName = req.body.barmanName
        var password = req.body.password
        var re_password = req.body.re_password
        var usertype = req.body.usertype
        var clubId = req.body.clubId
        if (password != re_password) {
            //CONTRASEÑAS NO COINCIDEN - DECIDIR - VER SI VA ACÁ
        }
        var newBarman = Barman({
            email: email,
            barmanName: barmanName,
            picture: "photo_user",
            password: password,
            usertype: usertype,
            clubId: clubId
        })
        var responseJSON = {
            message: 'Barman creado'
        }
        newBarman.save(function (err, barman) {
            if (err)
                return res.status(500).send({ message: err })
            responseJSON.email = barman[0].email
            responseJSON.usertype = barman[0].username
            responseJSON.picture = barman[0].picture
            responseJSON.clubId = barman[0].clubId
            responseJSON.barmanName = barman[0].barmanName
            req.session.email = responseJSON.email
            req.session.clubId = responseJSON.clubId
            req.session.usertype = responseJSON.usertype
            req.session.picture = responseJSON.picture
            req.session.barmanName = responseJSON.barmanName
            console.log(req.session)
            res.status(200).send(responseJSON)
        })
    },

    updateProfileImage: function () {

    },
    getProfileImage: function () {

    },
    updatePassword: function () {
    }
}