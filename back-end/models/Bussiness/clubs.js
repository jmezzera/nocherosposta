const clubDB = require('../DB/clubDBmgr')
const barmanDB = require('../DB/barmanDBmgr')
const fs = require('fs')
module.exports = {

    createClub: function(req,res){
      var email = clubDB.createClub(req,res,(email)=>{
        barmanDB.createFirstUser(req,res,email)    
    })
    },
    getClubs: function(req,res){
       return clubDB.getClubsFromDB(req,res)
    },
    getFullInfo: function(){
        return clubDB.getFullInfo();
    },
    uploadImage: function(){

    },
    uploadPromo: function(req,res,id){
        return clubDB.uploadPromo(req,res,id)
    },
    readQRCode: function(){

    },
    updateClubDescription: function(){

    },
    updateLocation: function(){

    },
    getPromos: function(){

    },
    getImages: function(clubName, callback){
        var dirName = __dirname + '/../../controllers/static/images/' + clubName;
        fs.readdir(dirName, (err, filenames) =>{
            if (err)
                console.log(err)
            else{
                callback(filenames);
            }
        })
    }



}