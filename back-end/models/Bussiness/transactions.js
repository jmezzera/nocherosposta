const transactionDB = require('../DB/transactionDBmgr')
module.exports = {

    createTransaction: function(req,res){
        return transactionDB.createTransaction(req,res)
    },
    requireTransactions: function(userId, clubId,callback,next){
       transactionDB.requireTransactions(userId, clubId,(response)=>{
            var promosId =[]
            for (promoId in response){
                var promo = {
                    promoId: response[promoId].promoId,
                    qr_url : response[promoId].qr_url
                }
                promosId.push(promo)
            }
            callback(promosId)
        })
    }
}