const userDB = require('../DB/userDBmgr')
module.exports = {

    authenticate: function(req,res) {
        return userDB.authenticate(req,res)
    },
    createUser: function(req,res) {
        return userDB.createUser(req,res)
    },
    setProfileImage: function() {

    },
    getProfileImage: function() {

    },
    changePassword: function() {

    }
}