var qr = require('qr-image');
var fs = require('fs');
const config = require('../../config');
const club = require('./clubs');
const promoDB = require('../DB/promoDBmgr')
module.exports = {
    addPromo: function (req, res) {
        console.log(req.body)
        promoDB.addPromo(req, res)
    },
    requestPromo: function (id, callback,next) {
        var promos = this.getPromos2((promos) => {
            var desiredPromo;
            for (index in promos) {
                var tempPromo = promos[index];
                if (tempPromo.id == id){
                    desiredPromo = tempPromo;
                    console.log("ESTOY ACA")
                }
            }
            var imgID = Math.pow(10, 17) * Math.random();
            var imgUrl = 'static/images/temp/' + imgID + '.png';
            var response = {
                imgUrl: config.server.substring(0,config.server.length -1) + ':' + config.port + '/' + imgUrl, //El substring es para sacarle la barra al final
                promo: desiredPromo
            }

            this.generateQR(desiredPromo, imgUrl)
            callback(response);
        });

        // Faltaría agregar a la BD la promo como pendiente 
    },
    generateQR: function (promo, imgUrl) {
        var promoString = JSON.stringify(promo);
        var ws = fs.createWriteStream(__dirname + '/../../controllers/' + imgUrl);
        var QRStream = qr.image(promoString);        
        QRStream.pipe(ws);
    },
    getPromos: function (req, res) {
        return promoDB.getPromos(req, res)
    },
    getPromosBarman: function (req, res) {
        return promoDB.getPromosBarman(req, res)
    },
    getRequestPromos: function(req,res){
        var promos = promoDB.getPromos2((promos) => {
            var requiredPromos=[];
            for (index in promos) {
                for(index2 in req.body.promos){
                    var tempPromo = {
                        promo: promos[index],
                    }
                    if (req.body.promos[index2].promoId == (tempPromo.promo._id)){
                        var promo = {
                            promo: tempPromo.promo,
                            qr_url: req.body.promos[index2].qr_url
                        }
                        requiredPromos.push(promo)
                    }
                }
            }
            var response = requiredPromos
            res.send(response)
        });
    },
    getPromos2: function (callback) {
        return promoDB.getPromos2(callback);
    }


}