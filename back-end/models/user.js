const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    username : {type: String, require: true},
    picture : String,
    password : {type: String, require: true},
    email:  {type: String, require: true,index: { unique: true }}
});

var User = mongoose.model('User', userSchema);
module.exports =  User//permite exportar el schema para poder utilizado desde cualquier otro lugar de la aplicacion.