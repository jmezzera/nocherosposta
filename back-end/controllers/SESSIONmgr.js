var sessionTable = [];

module.exports = {
    updatePushData: function(duuid, token){
        var duuidExisted = false;
        for(var index in sessionTable){
            var device = sessionTable[index];
            if(device.duuid == duuid){
                duuidExisted = true;
                device.pushToken = token;
            }
        }
        if(!duuidExisted){
            var newDevice = {
                'duuid': duuid,
                'pushToken': token
            }
            sessionTable.push(newDevice);
        }
        console.log(sessionTable);
    },
    getAllPushTokens: function(){
        var result = [];
        for(var index in sessionTable)
           result.push(sessionTable[index].pushToken);
        return result;
    },
    getSpecificDevice: function(duuid){
        var result;
        for(var index in sessionTable)
            if(sessionTable[index].duuid == duuid){
                result = sessionTable[index];
                break
            }
        return result;
    }

}