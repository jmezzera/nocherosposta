const express = require('express');
const config = require('../../config')
const fs = require('fs')
const api = express.Router();
const user = require('../../models/Bussiness/users')
const club = require('../../models/Bussiness/clubs')
const auth = require('../../middlewares/auth')
const barman = require('../../models/Bussiness/barmans')
const promo = require('../../models/Bussiness/promos')
const transaction = require('../../models/Bussiness/transactions')
const session = require('../SESSIONmgr')
const push = require('../PUSHmgr')

//para actualizar una promo de la base de datos

api.put('/userData/:profileImage',auth, (req,res) =>{
}) //modifica imagen perfil usuario con validacion de autenticacion del mismo 
api.post('/user/signup',user.createUser)
api.post('/user/signin',user.authenticate)
api.post('/barman/signup',barman.createUser)
api.post('/barman/signin',barman.authenticate)

api.get('/clubs',club.getClubs)//devurelve todos los boliches

api.get('/fullInfo', (req,res) =>{
     info = club.getFullInfo();
    res.send(info);
})

api.get('/logout', (req,res) =>{
}) //vuelve al mapa principal en modo usuario no loggueado 

api.put('/userData/:profileImage',auth,(req,res) =>{}) //modifica imagen perfil usuario con validacion de autenticacion del mismo 

api.put('/userData/:password',auth, (req,res) =>{}) //modifica contraseña usuario con validacion de autenticacion del mismo 

api.post('/club',club.createClub)
api.get('/clubs/:clubDir', (req,res) =>{}) //devuelve todos los boliches en una zona 

api.get('/clubs/:clubId/promos',promo.getPromos) //devuelve las promociones para un club - solo para user admin

api.get('/clubs/:clubId/:barmanId/promos',promo.getPromosBarman) //devuelve las promociones para un barman de un club

api.get('/clubs/:clubId/:clubPromId', auth, (req,res) =>{}) //verifica la autenticacion, y devuelve codigo de promoción seleccionada para ese club 

api.post('/clubs/getImages', (req, res) =>{
    var clubName = req.body.club;
    if (clubName == undefined)
        clubName = "Rancho"
    club.getImages(clubName, (images)=>{
        var imgUrlBase = config.server.substring(0, config.server.length - 1) + ':' + config.port + '/static/images/' + clubName + '/';
        for(index in images)
            images[index] = imgUrlBase + images[index];
        res.send(images.toString());
    }) 
})
api.post('/user/:userId/videos-fotos', auth, (req,res) =>{}) //verifica la autenticación y sube un video o foto para un usuario determinado 
/*
api.post('/requestPromo', (req,res) =>{
     id = req.body.promoId;
    //Faltaría pensar en el usuario que la pide. Probablemente se resuelva con tema de sesiones e info dentro del req
     responseText = promo.requestPromo(id, (responseText) =>{
        res.send(responseText);
    });

})*/
api.post('/requestPromo',(req,res,next) =>{
    id = req.body.promoId;
    clubId = req.body.clubId
    //Faltaría pensar en el usuario que la pide. Probablemente se resuelva con tema de sesiones e info dentro del req
     responseText = promo.requestPromo(id, (responseText) =>{
        req.body.qr_url = responseText.imgUrl
        req.body.promoName = responseText.promo.name
        next()
    })
},transaction.createTransaction)
api.get('/requestedPromos/:userId/:clubId',(req,res,next) =>{
     id = req.params.userId;
     clubId = req.params.clubId
     responseText = transaction.requireTransactions(id, clubId,(responseText) =>{
        req.body.promos = responseText
        next()
    })
},promo.getRequestPromos)
api.post('/addPromo',
    promo.addPromo
)//crea promo para un bar
api.post('/pushRegister', (req,res) =>{
    session.updatePushData(req.body.deviceuuid, req.body.tokenid);
})
api.post('/sendPush', (req,res)=>{
     msg = req.body.msg;
    push.sendSimpleMessageToAllUsers(msg);

})
api.post('/uploadImage', (req, res) => {
    var photo = req.body.photo;
    var clubName = req.body.clubName;
    console.log(req.body.photo)
    var imageRoute = __dirname + '/../static/images/' + clubName + '/';
    fs.readdir(imageRoute, (err, filenames)=>{
        if(err)
            console.log(err)
        else{
            var imageCount = filenames.length;
            imageCount++;
            console.log(imageRoute + imageCount + '.png')
            fs.writeFile(imageRoute + imageCount + '.png', photo, 'base64', function (err) {
                console.log("asdasdasdasdasd")
                console.log(err);
            })
        }
    })
})
module.exports = api
  
