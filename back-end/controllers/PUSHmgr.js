const gcm = require('node-gcm');
const senderKey = 'AAAAQA_Zpt0:APA91bGZXiV6spggqXl0IGyFaTRnOeGRyZIR7RypnRCQLMVvbyLNVMW-uu_qx1LpVlzZqp3qNOR_qXHtZoNtPqvIGIdOGi8MzuItsw-_UVTqXmEqvczsdoKswwQmamDK4wKv0T-V9StT';
const session = require('./SESSIONmgr')


var sender = new gcm.Sender(senderKey);


module.exports = {
    sendSimpleMessageToAllUsers: function(msg){
        var tokens = session.getAllPushTokens();
        var message = new gcm.Message({
            data: {message: msg}
        });
        sender.send(message, {registrationTokens: tokens}, function (err, res){
            if (err)
                console.error("Error enviando push" + err);
            else (console.log("PUSH enviado a todos los usuarios con el texto " + msg))
        })
    }
}