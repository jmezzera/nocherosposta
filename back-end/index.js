//DATABASE CONNECTION
const config = require('./config')
const mongoose = require('mongoose'); 
const app = require('./app')
var cfenv = require('cfenv');//permite el acceso a la nube de ibm
var appEnv = cfenv.getAppEnv();//obtiene las características del ambiente
/*
app.listen(appEnv.port,appEnv.bind, () => {
    mongoose.connect(config.db)
    console.log(`API REST CORRIENDO EN http://localhost:${config.port}`);
});*/
/*
//CONEXION PARA CLOUD
mongoose.connect(config.db,(err,res) => {//url con direccion de la base de datos
    if (err) 
        return console.log(`Error al intentar conectarse a la base de datos : ${err}`);
    console.log('Conexion a la base de datos establecida...');
    //una vez conectado queremos que se conecte a la API por eso incluimos app.listen dentro
    app.listen(appEnv.port,appEnv.bind, () => {
        console.log(`API REST CORRIENDO EN http://localhost:${config.port}`);
    });
});*/
//CONEXION LOCAL
mongoose.connect(config.db,(err,res) => {//url con direccion de la base de datos
    if (err) 
        return console.log(`Error al intentar conectarse a la base de datos : ${err}`);
    console.log('Conexion a la base de datos establecida...');
    //una vez conectado queremos que se conecte a la API por eso incluimos app.listen dentro
    app.listen(config.port, () => {
        console.log(`API REST CORRIENDO EN http://localhost:${config.port}`);
    });
});
