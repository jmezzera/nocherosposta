const express = require('express');
const body_parser = require('body-parser');//middleware

const session = require('express-session')
const MongoStore = require('connect-mongo')(session);
const app = express();
const config = require('./config')
app.use(session({  
    secret:"nocheros",
    store: new MongoStore({
       url: config.db
    }),
    cookie: true,
    saveUninitialized: true,
    resave: false,
}))
app.use(body_parser.urlencoded({
    parameterLimit: 100000,
    limit: '50mb',
    extended : true
}));//utilizo el metodo use para agregar el middleware a la app
app.use(body_parser.json());//esto permite peticiones con cuerpo de mensaje en formato json
app.use("/static/images", express.static(__dirname + '/controllers/static/images/'));
app.use("/static/images/temp", express.static(__dirname + '/controllers/static/images/temp'));
app.use("/static/images/drink_type", express.static(__dirname + '/controllers/static/images/drink_type'));
const api = require('./controllers/routes')
app.use('/api',api)
module.exports = app