module.exports = {
    //server: 'https://nocheros-api.mybluemix.net/', //CLOUD del faja
    server: 'http://190.64.48.66/', //UM 
    //server: 'http://localhost:3000/', //Localhost
    port: 3000,//process.env.PORT ,//Conectese a cualquier puerto o al 3000
    db: 'mongodb://localhost:27017/NocherosDB', //Local
    //db: 'mongodb://jpff96:jpff962018@nocherosdb-shard-00-00-tlpfl.mongodb.net:27017,nocherosdb-shard-00-01-tlpfl.mongodb.net:27017,nocherosdb-shard-00-02-tlpfl.mongodb.net:27017/test?ssl=true&replicaSet=NocherosDB-shard-0&authSource=admin',
    SECRET_TOKEN: 'miClaveDeTokens'//generalmente codigo mas complicado
}